# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/zopper/Android/Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# ButterKnife 6

-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**
-keep class **$$ViewInjector { *; }
-keepclasseswithmembernames class * {
    @butterknife.* <fields>;
}

-keepclasseswithmembernames class * {
    @butterknife.* <methods>;
}

-dontwarn com.squareup.okhttp.**

#keep entities
-keep class feed.brand.entities.** { *; }
-keep class feed.data.entities.** { *; }
-keep class feed.login.entities.** { *; }

-dontwarn com.matesnetwork.**

-dontwarn org.apache.**

# Quickblox

-keep class org.jivesoftware.smack.** { public *; }
-keep class org.jivesoftware.smackx.** { public *; }
-keep class com.quickblox.** { public *; }
-keep class * extends org.jivesoftware.smack { public *; }
-keep class * implements org.jivesoftware.smack.debugger.SmackDebugger { public *; }

-dontwarn com.quickblox.**

-dontwarn org.apache.**

-dontwarn org.xbill.**

#keep quickchat
-keep class com.adister.quickchat.** { *; }
-keep class com.quickblox.** { *; }

#keep support-kit
-keep class io.supportkit.** { *; }

#keep intercom
-keep class io.intercom.** { *; }

#Mixpanel
-dontwarn com.mixpanel.**

-keep class com.google.**
-dontwarn com.google.**

-dontwarn java.nio.**
-dontwarn org.codehaus.**

-keep class com.google.gson.** { *; }
-keep class com.google.inject.** { *; }
-keep class org.apache.http.** { *; }
-keep class org.apache.james.mime4j.** { *; }
-keep class javax.inject.** { *; }
-keep class retrofit.** { *; }
-dontwarn rx.**
-keepattributes Signature
-keep class sun.misc.** { *; }

-dontwarn org.apache.http.**
-dontwarn android.net.http.AndroidHttpClient
-dontwarn retrofit.client.ApacheClient$GenericEntityHttpRequest
-dontwarn retrofit.client.ApacheClient$GenericHttpRequest
-dontwarn retrofit.client.ApacheClient$TypedOutputEntity

-dontwarn sun.misc.**

-keepclassmembers class rx.internal.util.unsafe.*ArrayQueue*Field* {
   long producerIndex;
   long consumerIndex;
}

-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
   long producerNode;
   long consumerNode;
}

-keepattributes SourceFile,LineNumberTable,*Annotation*
-keep class com.crashlytics.android.**

-keep class com.crashlytics.** { *; }
-dontwarn com.crashlytics.**

-keep class com.google.android.gms.ads.identifier.** { *; }

# Design support library
-keep class android.support.design.widget.** { *; }
-keep interface android.support.design.widget.** { *; }
-dontwarn android.support.design.**