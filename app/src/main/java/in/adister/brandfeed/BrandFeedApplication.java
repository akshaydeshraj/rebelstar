package in.adister.brandfeed;

import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import feed.data.entities.FilterModel;
import in.adister.brandfeed.component.AppComponent;
import in.adister.brandfeed.component.DaggerAppComponent;
import in.adister.brandfeed.modules.AppModule;
import io.branch.referral.Branch;
import io.branch.referral.BranchApp;
import io.fabric.sdk.android.Fabric;
import io.supportkit.core.SupportKit;

/**
 * Created by zopper on 7/6/15.
 */
public class BrandFeedApplication extends BranchApp {

    private AppComponent component;

    private Tracker mTracker;

    public static BrandFeedApplication get(Context context) {
        return (BrandFeedApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Fabric.with(this, new Crashlytics());
        Branch.getAutoInstance(this);

        setupGraph();

        FacebookSdk.sdkInitialize(getApplicationContext());

        SupportKit.init(this, "e34bk87f2sqztlbt1jpr9oy83");

        FilterModel.getInstance().reset();
    }

    private void setupGraph() {
        component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
        component.inject(this);
    }

    public AppComponent component() {
        return component;
    }

    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            //mTracker = analytics.newTracker(R.xml.global_tracker);
            analytics.setLocalDispatchPeriod(1800);
            mTracker = analytics.newTracker("UA-68674652-1");
            mTracker.enableExceptionReporting(true);
            mTracker.enableAdvertisingIdCollection(true);
            mTracker.enableAutoActivityTracking(true);
        }
        return mTracker;
    }

}
