package in.adister.brandfeed;

/**
 * Created by zopper on 13/6/15.
 */
public class Constants {

    public static final String BASE_URL = "http://mybrandfeed.com/";

    public static final String IMAGE_URL = BASE_URL+"static/media/";

    public static final String PARAM_ALL = "posts";
    public static final String PARAM_HOT = "hotandnew";
    public static final String PARAM_COUPONS = "coupons";
    public static final String PARAM_STORIES = "stories";

    //Keys For Intent
    public static final String LOGGED_IN = "loggedIn";

    public static final String LOGIN_TYPE = "loginType";
    public static final String LOGIN_TYPE_FB = "fb";
    public static final String LOGIN_TYPE_NONE = "none";
    public static final String LOGIN_TYPE_GOOGLE = "gplus";

    public static final String USER_FB = "fbUser";

    public static final String ACTIVITY_LAUNCHER = "LauncherActivity";
    public static final String ACTIVITY_MAIN = "MainActivity";
    public static final String ACTIVITY_LOGIN = "LoginActivity";

    public static final String SOURCE = "source";

    public static final String USER_BRAND = "brandUser";
    public static final String BRAND_USER_SAVED = "isUserSaved";

    public static final String IMAGE = "image";

    public static final String PARENT_ACTIVITY = "parent_activity";
    public static final String SHARE_POST_ID = "post_id";
    public static final String SHARE_POST_URL = "post_url";
    public static final String SHARE_USER_ID = "user_id";

    //Keys for deep linkining
    public static final String BRANCH_IMAGE_URL = "branch_image_url";
    public static final String BRANCH_BRAND_IMAGE = "branch_brand_image";
    public static final String BRANCH_BRAND_NAME = "branch_brand_name";
    public static final String BRANCH_POST_TIME_AGO = "branch_post_time_ago";
    public static final String BRANCH_POST_TITLE = "branch_post_title";
    public static final String BRANCH_POST_CODE = "branch_post_code";
    public static final String BRANCH_POST_DESCRIPTION = "branch_post_description";
    public static final String BRANCH_POST_URL = "branch_post_url";
    public static final String BRANCH_POST_TYPE = "branch_post_type";
    public static final String BRANCH_POST_LIKES_COUNT = "branch_post_likes_count";
    public static final String BRANCH_POST_ID = "branch_post_id";
    public static final String BRANCH_BRAND_ID = "branch_brand_id";
    public static final String BRANCH_BRAND_CATEGORY_ID = "branch_brand_cat_id";

    public static final String INTENT_BRAND_ID = "brand_id";
    public static final String INTENT_BRAND_NAME = "brand_name";
}
