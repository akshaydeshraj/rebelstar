package in.adister.brandfeed.gcm;

import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 08-Nov-15
 */
public class BrandGcmListenerService extends GcmListenerService {

    private static final String TAG = BrandGcmListenerService.class.getSimpleName();

    @Override
    public void onMessageReceived(String from, Bundle data) {
        super.onMessageReceived(from, data);

        String message = data.getString("message");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);

    }
}
