package in.adister.brandfeed.gcm;

import android.app.IntentService;
import android.content.Intent;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import in.adister.brandfeed.R;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 08-Nov-15
 */
public class RegistrationIntentService extends IntentService {

    public RegistrationIntentService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        try{
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
