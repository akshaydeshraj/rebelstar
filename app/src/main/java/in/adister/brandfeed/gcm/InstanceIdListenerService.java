package in.adister.brandfeed.gcm;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 08-Nov-15
 */
public class InstanceIdListenerService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
