package in.adister.brandfeed.ui.login;

import java.util.Map;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 08-Aug-15
 */
public interface LoginPresenter {

    void addUser(String email, Map<String, String> queryMap);
}
