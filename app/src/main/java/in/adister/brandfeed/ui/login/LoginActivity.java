package in.adister.brandfeed.ui.login;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.util.Patterns;
import android.view.View;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import feed.data.entities.BrandUser;
import in.adister.brandfeed.BuildConfig;
import in.adister.brandfeed.Constants;
import in.adister.brandfeed.R;
import in.adister.brandfeed.component.AppComponent;
import in.adister.brandfeed.component.DaggerLoginComponent;
import in.adister.brandfeed.modules.LoginModule;
import in.adister.brandfeed.ui.common.BaseActivity;
import in.adister.brandfeed.ui.followBrands.FollowBrandsActivity;
import in.adister.brandfeed.ui.main.MainActivity;
import in.adister.brandfeed.utils.models.FbUser;
import in.adister.brandfeed.utils.models.GoogleUser;
import io.saeid.fabloading.LoadingView;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @author akshay
 * @version 1.0.0
 * @since 5/7/15
 */
public class LoginActivity extends BaseActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LoginView {

    private static final String TAG = "LoginActivity";
    static FbUser fbUser;

    @Inject
    LoginPresenter presenter;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    SharedPreferences.Editor editor;

    /* Request code used to invoke sign in user interactions. */
    private static final int RC_SIGN_IN = 0;

    /* Client used to interact with Google APIs. */
    private GoogleApiClient mGoogleApiClient;

    /* Is there a ConnectionResult resolution in progress? */
    private boolean mIsResolving = false;

    /* Should we automatically resolve ConnectionResults when possible? */
    private boolean mShouldResolve = false;

    String imageUrl = "";
    boolean isFirstTime;

    @InjectView(R.id.loading_view)
    LoadingView mLoadingView;

    @InjectView(R.id.loading_layout)
    View mLoadingLayout;

    private CallbackManager callbackManager;

    private boolean skippedLogin = false;

    private Map<String, String> loginQueryMap;

    private JSONObject props;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        ButterKnife.inject(this);

        callbackManager = CallbackManager.Factory.create();

        isFirstTime = sharedPreferences.getBoolean("first_time", true);

        boolean isLollipop = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
        int marvel_1 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_1;
        int marvel_2 = isLollipop ? R.drawable.marvel_2_lollipop : R.drawable.marvel_2;
        int marvel_3 = isLollipop ? R.drawable.marvel_3_lollipop : R.drawable.marvel_3;
        int marvel_4 = isLollipop ? R.drawable.marvel_4_lollipop : R.drawable.marvel_4;
        mLoadingView.addAnimation(Color.parseColor("#FFD200"), marvel_1,
                LoadingView.FROM_LEFT);
        mLoadingView.addAnimation(Color.parseColor("#2F5DA9"), marvel_2,
                LoadingView.FROM_TOP);
        mLoadingView.addAnimation(Color.parseColor("#FF4218"), marvel_3,
                LoadingView.FROM_RIGHT);
        mLoadingView.addAnimation(Color.parseColor("#C7E7FB"), marvel_4,
                LoadingView.FROM_BOTTOM);

        loginQueryMap = new HashMap<>();

        //Data to be sent with every event
        props = new JSONObject();
        try {
            props.put("AppVersion", BuildConfig.VERSION_NAME);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Build GoogleApiClient with access to basic profile
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(new Scope(Scopes.PROFILE))
                .addScope(new Scope(Scopes.EMAIL))
                .build();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName("Activity " + "Login");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    protected void onDestroy() {
        mixpanelAPI.flush();
        super.onDestroy();
    }

    @Override
    protected void setupComponent(AppComponent component) {

        DaggerLoginComponent.builder()
                .appComponent(component)
                .loginModule(new LoginModule(this))
                .build()
                .inject(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            // If the error resolution was not successful we should not resolve further.
            if (resultCode != RESULT_OK) {
                mShouldResolve = false;
            }
            mIsResolving = false;
            mGoogleApiClient.connect();

        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @OnClick(R.id.ib_fb_login)
    public void loginWithFacebook() {

        mLoadingLayout.setVisibility(View.VISIBLE);
        mLoadingView.startAnimation();

        LoginManager.getInstance().logInWithReadPermissions(this,
                Arrays.asList("email", "public_profile"));

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        Log.d("Login", "Success");
                        GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {

                                        if (graphResponse.getError() != null) {
                                            Snackbar.make(findViewById(android.R.id.content),
                                                    graphResponse.getError().getErrorMessage(),
                                                    Snackbar.LENGTH_INDEFINITE)
                                                    .show();
                                        } else {

                                            try {
                                                props.put("loginType", "fb");
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                            fbUser = new GsonBuilder().create().fromJson(
                                                    jsonObject.toString(), FbUser.class);

                                            imageUrl = "http://graph.facebook.com/" +
                                                    fbUser.getId() + "/picture?type=large";

                                            loginQueryMap.clear();
                                            loginQueryMap.put("first_name", fbUser.getFirstName());
                                            loginQueryMap.put("last_name", fbUser.getLastName());

                                            Log.d("RetroFb", jsonObject.toString());
                                            Log.d("RetroFb", fbUser.getFirstName() + " " +
                                                    fbUser.getLastName() + " " + fbUser.getEmail() + " " + fbUser.getId());
                                            presenter.addUser(fbUser.getEmail(), loginQueryMap);
                                        }
                                    }
                                });

                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "name,first_name,last_name,gender,email");

                        graphRequest.setParameters(parameters);
                        graphRequest.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Log.d("Fb", "cancelled");
                    }

                    @Override
                    public void onError(FacebookException e) {
                        Log.d("Fb", "hr" + e.toString());
                    }
                });
    }

    @OnClick(R.id.ib_gplus_login)
    public void loginWithGoogle() {

        // User clicked the sign-in button, so begin the sign-in process and automatically
        // attempt to resolve any errors that occur.
        mShouldResolve = true;
        mGoogleApiClient.connect();

        mLoadingLayout.setVisibility(View.VISIBLE);
        mLoadingView.startAnimation();
    }

    @OnClick(R.id.ib_skip_login)
    public void showMainActivity() {

        try {
            props.put("loginType", "skipped");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        skippedLogin = true;

        loginQueryMap.clear();

        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        Account[] accounts = AccountManager.get(this).getAccounts();
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                String possibleEmail = account.name;
                presenter.addUser(possibleEmail, loginQueryMap);
                break;
            }
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        // onConnected indicates that an account was selected on the device, that the selected
        // account has granted any requested permissions to our app and that we were able to
        // establish a service connection to Google Play services.
        mShouldResolve = false;

        GoogleUser googleUser;

        if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
            Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
            String email = Plus.AccountApi.getAccountName(mGoogleApiClient);

            googleUser = new GoogleUser(currentPerson, email);

            try {
                props.put("loginType", "google");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            imageUrl = googleUser.getImageUrl();

            loginQueryMap.clear();
            loginQueryMap.put("first_name", googleUser.getFirstName());
            loginQueryMap.put("last_name", googleUser.getLastName());
            presenter.addUser(googleUser.getEmail(), loginQueryMap);

        } else {
            // TODO : GoogleUser is null. Log to crashlytics
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // Could not connect to Google Play Services.  The user needs to select an account,
        // grant permissions or resolve an error in order to sign in. Refer to the javadoc for
        // ConnectionResult to see possible error codes.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);

        if (!mIsResolving && mShouldResolve) {
            if (connectionResult.hasResolution()) {
                try {
                    connectionResult.startResolutionForResult(this, RC_SIGN_IN);
                    mIsResolving = true;
                } catch (IntentSender.SendIntentException e) {
                    Log.e(TAG, "Could not resolve ConnectionResult.", e);
                    mIsResolving = false;
                    mGoogleApiClient.connect();
                }
            } else {

                Snackbar.make(findViewById(android.R.id.content),
                        connectionResult.getErrorMessage(), Snackbar.LENGTH_INDEFINITE)
                        .show();
            }
        } else {

        }
    }


    @Override
    public void showProgress() {
        mLoadingLayout.setVisibility(View.VISIBLE);
        mLoadingView.startAnimation();
    }

    @Override
    public void hideProgress() {
        mLoadingLayout.setVisibility(View.GONE);
    }

    @Override
    public void setSuccess(final BrandUser response, Response retrofitResponse) {

        mixpanelAPI.track("Signup", props);
        MixpanelAPI.People people = mixpanelAPI.getPeople();
        people.identify(response.getId());
        people.set("$email", response.getEmail());
        people.initPushHandling("236462081076");

        if (skippedLogin) {
            response.setLoggedIn(false);
            editor.putBoolean(Constants.LOGGED_IN, false);
        } else {
            response.setLoggedIn(true);
            editor.putBoolean(Constants.LOGGED_IN, true);

            people.set("$first_name", response.getFirstName());
            people.set("$last_name", response.getLastName());

        }

        //TODO: Image url and full name should be included in the response
        response.setImageUrl(imageUrl);

        Gson gson = new Gson();
        String brandUser = gson.toJson(response);
        editor.putBoolean(Constants.BRAND_USER_SAVED, true);
        editor.putString(Constants.USER_BRAND, brandUser);
        boolean completed = editor.commit();
        Log.v(TAG, brandUser);

        showNextActivity();
    }

    @Override
    public void setError(RetrofitError error) {

    }

    private void showNextActivity() {

        Intent intent;
        if (isFirstTime) {
            intent = new Intent(LoginActivity.this, FollowBrandsActivity.class);
            intent.putExtra(Constants.PARENT_ACTIVITY, "LoginActivity");
            editor.putBoolean("first_time", false);
            editor.apply();
        } else {
            intent = new Intent(LoginActivity.this, MainActivity.class);
        }
        startActivity(intent);
        finish();
    }
}