package in.adister.brandfeed.ui.customViews;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import in.adister.brandfeed.R;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 23-Aug-15
 */
public class SelectableTextView extends TextView implements View.OnClickListener {

    private Boolean selected = false;
    private String type = "brand"; //brand or male or female

    int paddingLeft, paddingRight, paddingTop, paddingBottom;

    int margin;

    public SelectableTextView(Context context) {
        super(context);

        paddingLeft = Math.round(convertDpToPixel(23, context));
        paddingRight = Math.round(convertDpToPixel(23, context));
        paddingTop = Math.round(convertDpToPixel(10, context));
        paddingBottom = Math.round(convertDpToPixel(10, context));

        margin = Math.round(convertDpToPixel(8, context));

        setOnClickListener(this);

        setGravity(Gravity.CENTER_VERTICAL);
    }

    public SelectableTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        setOnClickListener(this);

        setGravity(Gravity.CENTER_VERTICAL);

        TypedArray array = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.selectabletv,
                0, 0);

        try {
            selected = array.getBoolean(R.styleable.selectabletv_stv_selected, false);
            type = array.getString(R.styleable.selectabletv_stv_type);
        } finally {
            array.recycle();
        }

        paddingLeft = Math.round(convertDpToPixel(23, context));
        paddingRight = Math.round(convertDpToPixel(23, context));
        paddingTop = Math.round(convertDpToPixel(10, context));
        paddingBottom = Math.round(convertDpToPixel(10, context));

        margin = Math.round(convertDpToPixel(8, context));

        setTVSelected(selected);
    }

    public SelectableTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SelectableTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setTVSelected(boolean selected) {
        this.selected = selected;

        switch (type) {
            case "brand":
                if (selected) {
                    //Blue background, white text and tick drawable
                    setTextColor(Color.WHITE);
                    setBackgroundResource(R.drawable.box_selected_brand);
                    setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_white_24dp, 0);
                    setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
                } else {
                    //White background, black text and plus drawable
                    setTextColor(Color.BLACK);
                    setBackgroundResource(R.drawable.box_not_selected_brand);
                    setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                    setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
                }
                break;
            case "male":
                if (selected) {
                    setTextColor(Color.WHITE);
                    setBackgroundResource(R.drawable.box_selected_male);
                    setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_white_24dp, 0);
                    setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
                } else {
                    setTextColor(Color.WHITE);
                    setBackgroundResource(R.drawable.box_selected_male);
                    setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_white_24dp, 0);
                    setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
                }
                break;
            case "female":
                if (selected) {
                    setTextColor(Color.WHITE);
                    setBackgroundResource(R.drawable.box_selected_female);
                    setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_white_24dp, 0);
                    setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
                } else {
                    setTextColor(Color.WHITE);
                    setBackgroundResource(R.drawable.box_selected_female);
                    setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_white_24dp, 0);
                    setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
                }
                break;
        }
    }

    public boolean getTVSelected() {
        return selected;
    }

    public void setType(String type) {
        this.type = type;
    }

/*    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        //super.onLayout(changed, left, top, right, bottom);

        ViewGroup.MarginLayoutParams margins = ViewGroup.MarginLayoutParams.class.cast(getLayoutParams());
        margins.topMargin = 0;
        margins.bottomMargin = margin;
        margins.leftMargin = 0;
        margins.rightMargin = margin;
        setLayoutParams(margins);
    }*/

    @Override
    public void onClick(View v) {
        setTVSelected(!getTVSelected());
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp      A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }


}
