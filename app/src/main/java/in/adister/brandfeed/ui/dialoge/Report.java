package in.adister.brandfeed.ui.dialoge;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import in.adister.brandfeed.R;

public class Report extends DialogFragment {

  LayoutInflater inflater;
  View view;
  @InjectView(R.id.feedback)
  EditText feedback;
  private View snackView;

  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {

    inflater = getActivity().getLayoutInflater();
    view = inflater.inflate(R.layout.report_dialog, null);
    ButterKnife.inject(this, view);

    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    builder.setView(view);
    setCancelable(false);
    return builder.create();
  }

  @OnClick(R.id.ok_button)
  public void onclickOk() {
    Snackbar.make(snackView, "Thanks for your feedback", Snackbar.LENGTH_SHORT).show();
    dismiss();
  }

  public void initialize(View viewById) {
    snackView = viewById;
  }
}
