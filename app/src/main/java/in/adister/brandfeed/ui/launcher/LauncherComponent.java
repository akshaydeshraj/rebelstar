package in.adister.brandfeed.ui.launcher;

import dagger.Component;
import in.adister.brandfeed.ActivityScope;
import in.adister.brandfeed.component.AppComponent;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 14-Jul-15
 */

@ActivityScope
@Component(
        dependencies = AppComponent.class
)
public interface LauncherComponent {

    void inject(LauncherActivity activity);

}