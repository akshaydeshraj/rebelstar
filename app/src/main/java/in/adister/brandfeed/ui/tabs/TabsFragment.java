package in.adister.brandfeed.ui.tabs;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.google.android.gms.analytics.HitBuilders;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import feed.data.entities.BrandUser;
import feed.data.entities.FilterModel;
import feed.data.entities.Post;
import feed.data.entities.StoriesFilterModel;
import in.adister.brandfeed.BrandFeedApplication;
import in.adister.brandfeed.BuildConfig;
import in.adister.brandfeed.Constants;
import in.adister.brandfeed.R;
import in.adister.brandfeed.adapters.RecyclerViewAdapter;
import in.adister.brandfeed.component.AppComponent;
import in.adister.brandfeed.component.DaggerTabsComponent;
import in.adister.brandfeed.modules.TabsModule;
import in.adister.brandfeed.ui.OfferDetailsActivity;
import in.adister.brandfeed.ui.common.BaseFragment;
import in.adister.brandfeed.ui.common.DeviceUtils;
import in.adister.brandfeed.ui.common.PreCachingLayoutManager;
import in.adister.brandfeed.ui.customViews.CustomImageButton;
import in.adister.brandfeed.ui.main.MainActivity;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by zopper on 6/6/15.
 */
public class TabsFragment extends BaseFragment implements TabsView {

    public static final int REQUEST_POST_DETAILS = 120;
    private static final String TAG = TabsFragment.class.getSimpleName();

    @Inject
    TabsPresenter presenter;

    @Inject
    Picasso picasso;

    @Inject
    BrandUser brandUser;

    @Inject
    SharedPreferences.Editor editor;

    @Inject
    BrandFeedApplication brandFeedApplication;

    @InjectView(R.id.rvItems)
    RecyclerView rvItems;

    String param;

    Map<String, String> queryMap = new HashMap<>();
    FilterModel filterModel;
    StoriesFilterModel storiesFilterModel;
    BrandUser brandUserClone;
    RecyclerViewAdapter adapter;

    List<Post> itemsResponse = new ArrayList<>();

    Post currentPost;
    private CustomImageButton cibFollowBrand;
    private String brandId;

    //For pagination
    int pageNumber = 1;
    private boolean loading = false;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    private JSONObject props;

    public static TabsFragment newInstance(String param) {

        TabsFragment tabsFragment = new TabsFragment();

        Bundle args = new Bundle();
        args.putString("param", param);
        tabsFragment.setArguments(args);

        return tabsFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        param = getArguments() != null ? getArguments().getString("param") : "stories";

        cloneFilterModel();
        cloneBrandUser();

        adapter = new RecyclerViewAdapter(getActivity(), picasso, brandUser);
        adapter.setParameter(param);
        adapter.setFragment(this);
        adapter.setItems(itemsResponse);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tab, container, false);

        ButterKnife.inject(this, rootView);

        //Fetch first page
        queryMap.clear();
        queryMap.put("type", param);
        queryMap.put("page", String.valueOf(pageNumber));

        queryMap.put("userId", brandUser.getId());

        presenter.init(param, queryMap);

        //Data to be sent with every event
        props = new JSONObject();
        try {
            props.put("AppVersion", BuildConfig.VERSION_NAME);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Start tracking
        mixpanelAPI.timeEvent("FetchData");

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final PreCachingLayoutManager layoutManager = new PreCachingLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setExtraLayoutSpace(DeviceUtils.getScreenHeight(getActivity()));
        rvItems.setLayoutManager(layoutManager);

        rvItems.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                visibleItemCount = layoutManager.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                if (layoutManager.findLastCompletelyVisibleItemPosition() == itemsResponse.size() - 1) {

                    if (!loading) {

                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = true;
                            pageNumber++;
                            queryMap.put("page", String.valueOf(pageNumber));
                            presenter.init(param, queryMap);
                        }

                    }
                }

            }
        });

        rvItems.setAdapter(adapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_POST_DETAILS && resultCode == Activity.RESULT_OK) {

            try {
                itemsResponse.get(itemsResponse.indexOf(currentPost)).
                        setUserLikesThisPost(data.getBooleanExtra("userLiked", false));

                itemsResponse.get(itemsResponse.indexOf(currentPost)).
                        setLikesCount(String.valueOf(data.getIntExtra("likesCount", 0)));

                refreshBrandFollowedStatus(currentPost.getBrandId(), data.getBooleanExtra("isFollowed", false));

                adapter.notifyItemChanged(itemsResponse.indexOf(currentPost));

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else

            try {
                if (!filterModel.isEqualTo(FilterModel.getInstance())) {

                    pageNumber = 1;

                    cloneFilterModel();

                    if (requestCode == MainActivity.REQUEST_CODE_FILTERS && resultCode == Activity.RESULT_OK
                            && !param.equals("stories")) {

                        queryMap.put("interest_id",
                                buildBrandFilterQueryFromCategoryId(FilterModel.getInstance().getCategories(), ",", param));
                        queryMap.put("page", String.valueOf(pageNumber));

                        adapter.clear();
                    }

                    presenter.init(param, queryMap);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        try {
            if (!storiesFilterModel.isEqualTo(StoriesFilterModel.getInstance())) {

                pageNumber = 1;

                cloneFilterModel();

                if (requestCode == MainActivity.REQUEST_CODE_FILTERS && resultCode == Activity.RESULT_OK
                        && !param.equals("coupons")) {

                    queryMap.put("interest_id",
                            buildBrandFilterQueryFromCategoryId(StoriesFilterModel.getInstance().getCategories(), ",", param));
                    queryMap.put("page", String.valueOf(pageNumber));

                    adapter.clear();
                }

                presenter.init(param, queryMap);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (filterModel.isEqualTo(FilterModel.getInstance()) &&
                !brandUserClone.isEqualTo(brandUser)) {

            cloneBrandUser();
            queryMap.clear();
            queryMap.put("userId", brandUser.getId());
            // queryMap.put("brand_id", buildBrandFilterQuery(brandUser.getBrands(), ","));
            presenter.init(param, queryMap);
        }

        mTracker.setScreenName("Tab " + param);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    protected void setupComponent(AppComponent appComponent) {

        DaggerTabsComponent.builder()
                .appComponent(appComponent)
                .tabsModule(new TabsModule(this))
                .build()
                .inject(this);
    }

    /**
     * The purpose of this method is to avoid unnecessary network calls. At the creation of this
     * fragment, the FilterModel is cloned. The network call in onActivityResult() is made only if
     * there are any changes to the FilterModel singleton. This method is then called again
     */
    public void cloneFilterModel() {
        try {
            filterModel = (FilterModel) FilterModel.getInstance().clone();
            storiesFilterModel = (StoriesFilterModel) StoriesFilterModel.getInstance().clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            filterModel = FilterModel.getInstance();
            storiesFilterModel = StoriesFilterModel.getInstance();
        }
    }

    public void cloneBrandUser() {
        try {
            brandUserClone = (BrandUser) brandUser.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void followBrand(final String brandId, final CustomImageButton cibFollowBrand) {

        this.cibFollowBrand = cibFollowBrand;
        this.brandId = brandId;

        if (cibFollowBrand.getState()) {

            //show a popup menu
            PopupMenu popupMenu = new PopupMenu(getActivity(), cibFollowBrand);
            popupMenu.getMenuInflater().inflate(R.menu.details_popup_menu, popupMenu.getMenu());

            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.menu_offer_unfollow:
                            cibFollowBrand.setState(!cibFollowBrand.getState());
                            presenter.followBrand(brandUser.getId(), brandId, cibFollowBrand.getState());
                            break;
                    }
                    return true;
                }
            });

            popupMenu.show();

        } else {
            cibFollowBrand.setState(!cibFollowBrand.getState());
            presenter.followBrand(brandUser.getId(), brandId, cibFollowBrand.getState());
        }


    }

    public void openDetailsActivity(Post post) {

        currentPost = post;

        Intent intent = new Intent(getActivity(), OfferDetailsActivity.class);
        intent.putExtra(OfferDetailsActivity.POST, post);
        intent.putExtra(OfferDetailsActivity.PARAM, post.getPostType());
        intent.putExtra(OfferDetailsActivity.VIA_LINK, false);
        startActivityForResult(intent, REQUEST_POST_DETAILS);


        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentName("DetailActivity")
                .putContentType(post.getPostType())
                .putContentId(post.getId())
                .putCustomAttribute("BrandId", post.getBrandId())
                .putCustomAttribute("BrandName", post.getBrand().getName()));

        try {
            props.put("postType", post.getPostType());
            props.put("postMrp", post.getMrp());
            props.put("brandId", brandId);
            props.put("interest", post.getBrand().getCategoryId());
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        mixpanelAPI.track("postClick", props);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void setSuccessLayout(List<Post> itemsResponse, Response retrofitResponse) {

        loading = false;

        Log.i(TAG, "Size " + String.valueOf(this.itemsResponse.size()));

        if (!itemsResponse.equals(adapter.getItems())) {
            adapter.addItems(itemsResponse);

            try {
                props.put("postType", param);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            mixpanelAPI.track("postFetched", props);
        }

        try {
            mixpanelAPI.track("FetchData");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setSuccessLayout(Response retrofitResponse) {

        if (cibFollowBrand.getState()) {
            /*Toast.makeText(getActivity(), "Followed brand "+brandId+" successfully",
                    Toast.LENGTH_SHORT).show();*/
            brandUser.addBrand(brandId);
        } else {
            /*Toast.makeText(getActivity(), "Unfollowed brand "+brandId+" successfully",
                    Toast.LENGTH_SHORT).show();*/
            brandUser.removeBrand(brandId);
        }

        String json = new Gson().toJson(brandUser);
        editor.putString(Constants.USER_BRAND, json);
        boolean completed = editor.commit();
    }

    @Override
    public void setErrorLayout(RetrofitError retrofitError) {

        if (retrofitError.getUrl().contains("interest_id")) {

            if (param.equalsIgnoreCase("stories")) {
                Snackbar.make(getActivity().findViewById(android.R.id.content),
                        "No story for this filter", Snackbar.LENGTH_LONG)
                        .show();
            } else {
                Snackbar.make(getActivity().findViewById(android.R.id.content),
                        "No coupons for this interest", Snackbar.LENGTH_LONG)
                        .show();
            }
            queryMap.put("interest_id", null);
            queryMap.put("type", null);
            presenter.init(param, queryMap);
        }

        Log.i(TAG, retrofitError.getLocalizedMessage());
    }

    public String buildBrandFilterQuery(List<BrandUser.FollowedBrand> brandList, String delimiter) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < brandList.size(); i++) {
            String s = brandList.get(i).getBrandId();
            if (s != null) {
                builder.append(s).append(delimiter);
            }
        }

        //To prevent Index out of bounds exception
        if (builder.length() == 0) {
            return null;
        } else {
            builder.setLength(builder.length() - delimiter.length());
            return builder.toString();
        }
    }

    private String buildBrandFilterQueryFromCategoryId(List<String> categories, String delimiter, String type) {

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < categories.size(); i++) {
            builder.append(categories.get(i)).append(delimiter);
        }

        //To prevent Index out of bounds exception
        if (builder.length() == 0) {
            queryMap.put("type", null);
            return null;
        } else {
            builder.setLength(builder.length() - delimiter.length());
            queryMap.put("type", type);
            return builder.toString();
        }
    }

    public void refreshBrandFollowedStatus(String brandId, boolean isFollowed) {

        Post post;

        if (isFollowed) {
            brandUser.addBrand(brandId);
        } else {
            brandUser.removeBrand(brandId);
        }

        Gson gson = new Gson();
        String brandUserString = gson.toJson(brandUser);
        editor.putBoolean(Constants.BRAND_USER_SAVED, true);
        editor.putString(Constants.USER_BRAND, brandUserString);
        boolean completed = editor.commit();

        Log.i("BugFollowedTab", brandId + " " + brandUser.contains(brandId));

        for (int i = 0; i < itemsResponse.size(); i++) {
            post = itemsResponse.get(i);
            if (post.getBrandId().equals(brandId)) {
                adapter.notifyItemChanged(i);
            }
        }
    }
}