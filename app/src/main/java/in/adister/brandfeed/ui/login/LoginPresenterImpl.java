package in.adister.brandfeed.ui.login;

import java.util.Map;

import feed.data.OnRequestFinishedListener;
import feed.data.entities.BrandUser;
import feed.data.rest.RestDataSource;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 08-Aug-15
 */

public class LoginPresenterImpl implements LoginPresenter, OnRequestFinishedListener {

    private LoginView view;
    private RestDataSource restDataSource;

    public LoginPresenterImpl(LoginView view, RestDataSource restDataSource) {
        this.view = view;
        this.restDataSource = restDataSource;
        restDataSource.setOnRequestFinishedListener(this);
    }

    @Override
    public void addUser(String email, Map<String, String> queryMap) {
        view.showProgress();
        restDataSource.addUser(email, queryMap);
    }

    @Override
    public void onSuccess(Object response, Response retrofitResponse) {

        view.hideProgress();
        view.setSuccess((BrandUser) response, retrofitResponse);
    }

    @Override
    public void onError(RetrofitError error) {

        view.hideProgress();
        view.setError(error);
    }
}