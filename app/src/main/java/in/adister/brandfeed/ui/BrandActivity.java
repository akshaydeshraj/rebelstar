package in.adister.brandfeed.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import dagger.Component;
import feed.data.OnRequestFinishedListener;
import feed.data.entities.BrandUser;
import feed.data.entities.Post;
import feed.data.rest.RestDataSource;
import in.adister.brandfeed.ActivityScope;
import in.adister.brandfeed.BuildConfig;
import in.adister.brandfeed.Constants;
import in.adister.brandfeed.R;
import in.adister.brandfeed.adapters.RecyclerViewAdapter;
import in.adister.brandfeed.component.AppComponent;
import in.adister.brandfeed.ui.common.BaseActivity;
import in.adister.brandfeed.ui.common.DeviceUtils;
import in.adister.brandfeed.ui.common.PreCachingLayoutManager;
import in.adister.brandfeed.ui.customViews.CustomImageButton;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 08-Nov-15
 */
public class BrandActivity extends BaseActivity implements OnRequestFinishedListener {

    private static final String TAG = BrandActivity.class.getSimpleName();

    public static final int REQUEST_POST_DETAILS = 120;

    @Inject
    RestDataSource restDataSource;

    @Inject
    Picasso picasso;

    @Inject
    BrandUser brandUser;

    @Inject
    SharedPreferences.Editor editor;

    @InjectView(R.id.rvItems)
    RecyclerView rvItems;

    @InjectView(R.id.app_bar)
    Toolbar toolbar;

    Map<String, String> queryMap = new HashMap<>();

    List<Post> itemsResponse = new ArrayList<>();
    RecyclerViewAdapter adapter;

    //For pagination
    int pageNumber = 1;
    private boolean loading = false;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    private Post currentPost;
    String brandId, brandName;

    CustomImageButton cibFollowBrand;
    private JSONObject props;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand);
        ButterKnife.inject(this);

        Intent intent = getIntent();
        brandId = intent.getStringExtra(Constants.INTENT_BRAND_ID);
        brandName = intent.getStringExtra(Constants.INTENT_BRAND_NAME);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(brandName);
        }

        adapter = new RecyclerViewAdapter(this, picasso, brandUser);
        adapter.setParameter("brand");
        adapter.setSource(TAG);
        adapter.setActivity(this);
        adapter.setItems(itemsResponse);

        queryMap.clear();
        queryMap.put("brand_id", brandId);
        queryMap.put("page", String.valueOf(pageNumber));
        restDataSource.setOnRequestFinishedListener(this);

        restDataSource.getPostsByBrand(queryMap);

        final PreCachingLayoutManager layoutManager = new PreCachingLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setExtraLayoutSpace(DeviceUtils.getScreenHeight(this));
        rvItems.setLayoutManager(layoutManager);

        rvItems.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                visibleItemCount = layoutManager.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                if (layoutManager.findLastCompletelyVisibleItemPosition() == itemsResponse.size() - 1) {

                    if (!loading) {

                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = true;
                            pageNumber++;
                            queryMap.put("page", String.valueOf(pageNumber));
                            restDataSource.getPostsByBrand(queryMap);
                        }
                    }
                }
            }
        });

        rvItems.setAdapter(adapter);

        //Data to be sent with every event
        props = new JSONObject();
        try {
            props.put("AppVersion", BuildConfig.VERSION_NAME);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_POST_DETAILS && resultCode == Activity.RESULT_OK) {

            try {
                itemsResponse.get(itemsResponse.indexOf(currentPost)).
                        setUserLikesThisPost(data.getBooleanExtra("userLiked", false));

                itemsResponse.get(itemsResponse.indexOf(currentPost)).
                        setLikesCount(String.valueOf(data.getIntExtra("likesCount", 0)));

                refreshBrandFollowedStatus(currentPost.getBrandId(), data.getBooleanExtra("isFollowed", false));

                adapter.notifyItemChanged(itemsResponse.indexOf(currentPost));

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSuccess(Object response, Response retrofitResponse) {

        loading = false;

        if (response instanceof List) {

            List<Post> postResponse = (List<Post>) response;

            if (!postResponse.equals(adapter.getItems())) {
                adapter.addItems(postResponse);

                try {
                    props.put("postType", "brand");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                mixpanelAPI.track("postFetched", props);
            }
        } else {
            if (cibFollowBrand.getState()) {
                brandUser.addBrand(brandId);
            } else {
                brandUser.removeBrand(brandId);
            }

            String json = new Gson().toJson(brandUser);
            editor.putString(Constants.USER_BRAND, json);
            boolean completed = editor.commit();
        }
    }

    @Override
    public void onError(RetrofitError error) {

    }

    public void openDetailsActivity(Post post) {

        currentPost = post;

        Intent intent = new Intent(this, OfferDetailsActivity.class);
        intent.putExtra(OfferDetailsActivity.POST, post);
        intent.putExtra(OfferDetailsActivity.PARAM, post.getPostType());
        intent.putExtra(OfferDetailsActivity.VIA_LINK, false);
        startActivityForResult(intent, REQUEST_POST_DETAILS);


        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentName("DetailActivity")
                .putContentType(post.getPostType())
                .putContentId(post.getId())
                .putCustomAttribute("BrandId", post.getBrandId())
                .putCustomAttribute("BrandName", post.getBrand().getName()));

        try {
            props.put("postType", post.getPostType());
            props.put("postMrp", post.getMrp());
            props.put("brandId", brandId);
            props.put("interest", post.getBrand().getCategoryId());
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        mixpanelAPI.track("postClick", props);
    }

    public void refreshBrandFollowedStatus(String brandId, boolean isFollowed) {

        Post post;

        if (isFollowed) {
            brandUser.addBrand(brandId);
        } else {
            brandUser.removeBrand(brandId);
        }

        Gson gson = new Gson();
        String brandUserString = gson.toJson(brandUser);
        editor.putBoolean(Constants.BRAND_USER_SAVED, true);
        editor.putString(Constants.USER_BRAND, brandUserString);
        boolean completed = editor.commit();

        Log.i("BugFollowedTab", brandId + " " + brandUser.contains(brandId));

        for (int i = 0; i < itemsResponse.size(); i++) {
            post = itemsResponse.get(i);
            if (post.getBrandId().equals(brandId)) {
                adapter.notifyItemChanged(i);
            }
        }
    }

    public void followBrand(final String brandId, final CustomImageButton cibFollowBrand) {

        this.cibFollowBrand = cibFollowBrand;
        this.brandId = brandId;

        if (cibFollowBrand.getState()) {

            //show a popup menu
            PopupMenu popupMenu = new PopupMenu(this, cibFollowBrand);
            popupMenu.getMenuInflater().inflate(R.menu.details_popup_menu, popupMenu.getMenu());

            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.menu_offer_unfollow:
                            cibFollowBrand.setState(!cibFollowBrand.getState());
                            restDataSource.followBrand(brandUser.getId(), brandId, cibFollowBrand.getState());
                            break;
                    }
                    return true;
                }
            });

            popupMenu.show();

        } else {
            cibFollowBrand.setState(!cibFollowBrand.getState());
            restDataSource.followBrand(brandUser.getId(), brandId, cibFollowBrand.getState());
        }

    }

    @ActivityScope
    @Component(
            dependencies = AppComponent.class
    )
    public interface BrandActivityComponent {

        void inject(BrandActivity activity);
    }

    @Override
    protected void setupComponent(AppComponent component) {
        DaggerBrandActivity_BrandActivityComponent.builder()
                .appComponent(component)
                .build()
                .inject(this);
    }
}
