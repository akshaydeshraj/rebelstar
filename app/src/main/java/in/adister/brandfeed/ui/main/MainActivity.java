package in.adister.brandfeed.ui.main;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import feed.data.entities.BrandUser;
import feed.data.entities.FilterModel;
import in.adister.brandfeed.BuildConfig;
import in.adister.brandfeed.Constants;
import in.adister.brandfeed.DateFormatter;
import in.adister.brandfeed.R;
import in.adister.brandfeed.adapters.ViewPagerAdapter;
import in.adister.brandfeed.component.AppComponent;
import in.adister.brandfeed.component.DaggerMainComponent;
import in.adister.brandfeed.modules.MainModule;
import in.adister.brandfeed.ui.FiltersActivity;
import in.adister.brandfeed.ui.StoriesFilterActivity;
import in.adister.brandfeed.ui.common.BaseActivity;
import in.adister.brandfeed.ui.followBrands.FollowBrandsActivity;
import in.adister.brandfeed.ui.login.LoginActivity;
import in.adister.brandfeed.ui.tabs.TabsFragment;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.BranchShortLinkBuilder;
import io.supportkit.core.User;
import io.supportkit.ui.ConversationActivity;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

public class MainActivity extends BaseActivity implements MainView {

    public static final int REQUEST_CODE_FILTERS = 113;
    private static final String TAG = MainActivity.class.getSimpleName();
    public static CallbackManager callbackManager;
    private static long back_pressed;

    @InjectView(R.id.app_bar)
    Toolbar mToolBar;
    @InjectView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @InjectView(R.id.nav_view)
    NavigationView navigationView;

    @Inject
    SharedPreferences preferences;
    @Inject
    Editor editor;
    @Inject
    Picasso picasso;
    @Inject
    BrandUser brandUser;

    @InjectView(R.id.tabs)
    TabLayout tabLayout;
    @InjectView(R.id.main_view_pager)
    ViewPager viewPager;
    @InjectView(R.id.avatar)
    CircleImageView circleImageView;
    @InjectView(R.id.tv_header_name)
    TextView tvUserName;
    @InjectView(R.id.fab_filter)
    FloatingActionButton fabFilter;
    ViewPagerAdapter adapter;
    private int height, initialPage = 0;
    private float absoluteTop;

    private JSONObject props;

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp      A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button cardCenter = (Button) findViewById(R.id.card_center);

        new MaterialShowcaseView.Builder(this)
                .setTarget(cardCenter)
                .setDismissText("GOT IT")
                .setContentText("Click on a card to see more details.")
                .setDelay(500) // optional but starting animations immediately in onCreate can make them choppy
                .singleUse("Card Center") // provide a unique ID used to ensure it is only shown once
                .setDismissOnTouch(true)
                .show();

        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        ButterKnife.inject(this);

        setSupportActionBar(mToolBar);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }

        //Data to be sent with every event
        props = new JSONObject();
        try {
            props.put("AppVersion", BuildConfig.VERSION_NAME);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject superProps = new JSONObject();

        try {
            if (brandUser.isLoggedIn()) {
                picasso.load(brandUser.getImageUrl()).fit().centerCrop().into(circleImageView);
                tvUserName.setText(brandUser.getFirstName() + " " + brandUser.getLastName());
                superProps.put("loggedIn", true);
                superProps.put("name", brandUser.getName());
                superProps.put("email", brandUser.getEmail());
                superProps.put("createdAt", brandUser.getCreatedAt());
                superProps.put("updatedAt", brandUser.getUpdatedAt());
            } else {
                superProps.put("loggedIn", true);
                //TODO : show generic image
                tvUserName.setText("Hello");
            }
            mixpanelAPI.registerSuperProperties(superProps);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        callbackManager = CallbackManager.Factory.create();

        setupSupport();

        if (viewPager != null) {
            setupViewPager(viewPager);
        }

        tabLayout.setBackgroundColor(Color.TRANSPARENT);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected void setupComponent(AppComponent component) {
        DaggerMainComponent.builder()
                .appComponent(component)
                .mainModule(new MainModule(this, this))
                .build()
                .inject(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.menu_chat:
                ConversationActivity.show(this);
                break;
            case R.id.menu_filter:
                Intent followBrandsIntent = new Intent(this, FollowBrandsActivity.class);
                followBrandsIntent.putExtra(Constants.PARENT_ACTIVITY, "MainActivity");
                startActivity(followBrandsIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_FILTERS && resultCode == RESULT_OK) {
            for (Fragment fragment : adapter.getFragments()) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        } else if (requestCode == TabsFragment.REQUEST_POST_DETAILS && resultCode == RESULT_OK) {
            adapter.getItem(0).onActivityResult(requestCode, resultCode, data);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onDestroy() {
        FilterModel.getInstance().reset();
        mixpanelAPI.flush();
        super.onDestroy();
    }

    @OnClick(R.id.fab_filter)
    public void startFiltersActivity() {

        Intent intent;

        if(viewPager.getCurrentItem() == 1){
            intent = new Intent(this, FiltersActivity.class);
            startActivityForResult(intent, REQUEST_CODE_FILTERS);
        }else{
            intent = new Intent(this, StoriesFilterActivity.class);
            startActivityForResult(intent, REQUEST_CODE_FILTERS);
        }

        try {
            props.put("UserLoggedIn", brandUser.isLoggedIn());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanelAPI.track("FiltersActivityOpened", props);
    }

    private void setupDrawerContent(NavigationView navigationView) {

        MenuItem logout = navigationView.getMenu().findItem(R.id.menu_logout);
        MenuItem login = navigationView.getMenu().findItem(R.id.menu_login);
        if (!preferences.getBoolean(Constants.LOGGED_IN, false)) {
            logout.setVisible(false);
        } else {
            login.setVisible(false);
        }

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(final MenuItem menuItem) {
                        menuItem.setChecked(false);
                        mDrawerLayout.closeDrawers();

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                performDrawerMenuAction(menuItem);
                            }
                        }, 200);
                        return true;
                    }
                });
    }

    private void performDrawerMenuAction(MenuItem menuItem) {

        switch (menuItem.getItemId()) {

            case R.id.item_follow_brands:
                Intent followBrandsIntent = new Intent(this, FollowBrandsActivity.class);
                followBrandsIntent.putExtra(Constants.PARENT_ACTIVITY, "MainActivity");
                startActivity(followBrandsIntent);
                break;
            case R.id.item_chat:
                ConversationActivity.show(this);
                break;
            case R.id.menu_logout:
                logout();
                break;
            case R.id.menu_login:
                Intent loginIntent = new Intent(this, LoginActivity.class);
                startActivity(loginIntent);
                this.finish();
                break;
            case R.id.menu_share_app:
                createShortLink();
                break;
        }
    }

    public void share(String url) {
        String shareText = "Check out RebelStar app. I think you'll like it. Here's the google play link- " + url;
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, shareText);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    public void createShortLink() {
        if (brandUser.isLoggedIn()) {
            BranchShortLinkBuilder shortUrlBuilder = new BranchShortLinkBuilder(this);
            if (brandUser.isLoggedIn()) {
                shortUrlBuilder.addParameters(Constants.SHARE_USER_ID, brandUser.getId());
            }
            shortUrlBuilder.generateShortUrl(new Branch.BranchLinkCreateListener() {
                @Override
                public void onLinkCreate(String url, BranchError error) {
                    if (error != null) {
                        Log.e(TAG, "Branch create short url failed. Caused by -" + error.getMessage());
                    } else {
                        share(url);
                    }
                }
            });
        } else {
            share("https://bnc.lt/m/DImoK5GFEn");
        }
    }

    private void setupViewPager(final ViewPager viewPager) {
        viewPager.setOffscreenPageLimit(1);
        adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(TabsFragment.newInstance(Constants.PARAM_STORIES), "Stories");
        adapter.addFragment(TabsFragment.newInstance(Constants.PARAM_COUPONS), "Offers");
        viewPager.setAdapter(adapter);
    }

    private void logout() {

        if (preferences.getString(Constants.LOGIN_TYPE, "").equals(Constants.LOGIN_TYPE_FB)) {
            LoginManager.getInstance().logOut();
        }
        editor.putBoolean(Constants.LOGGED_IN, false);
        boolean completed = editor.commit();

        Intent loginIntent = new Intent(this, LoginActivity.class);
        startActivity(loginIntent);
        this.finish();
    }

    private void setupSupport() {

        Crashlytics.getInstance().core.setString("UserCreatedAt", brandUser.getCreatedAt());

        User skUser = User.getCurrentUser();

        skUser.setFirstName(brandUser.getFirstName());
        skUser.setLastName(brandUser.getLastName());
        skUser.setEmail(brandUser.getEmail());

        final Map<String, Object> customProperties = new HashMap<>();

        customProperties.put("Created At", DateFormatter.getTimeAgo(brandUser.getCreatedAt()));
        customProperties.put("image", brandUser.getImageUrl());

        skUser.addProperties(customProperties);


    }

    @Override
    public void onBackPressed() {
        if (back_pressed + 2000 > System.currentTimeMillis()) {
            super.onBackPressed();
            finish();
        } else {
            Toast.makeText(getBaseContext(), "Press once again to exit!", Toast.LENGTH_SHORT).show();
            back_pressed = System.currentTimeMillis();
        }
    }

    public BrandUser getBrandUser() {
        return brandUser;
    }
}

