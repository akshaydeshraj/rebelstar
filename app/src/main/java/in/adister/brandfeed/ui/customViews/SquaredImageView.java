package in.adister.brandfeed.ui.customViews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * @author akshay
 * @version 1.0.0
 * @since 30/6/15
 */
public class SquaredImageView extends ImageView {

    public SquaredImageView(Context context){
        super(context);
    }

    public SquaredImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquaredImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width = getMeasuredWidth();

        //force a 4:3 aspect ratio
        int height = Math.round(width * .75f);

        setMeasuredDimension(width, height);
    }
}
