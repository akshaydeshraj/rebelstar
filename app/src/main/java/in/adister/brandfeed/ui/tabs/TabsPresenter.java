package in.adister.brandfeed.ui.tabs;

import java.util.Map;

/**
 * Created by zopper on 13/6/15.
 */
public interface TabsPresenter {

    void init(String param, Map<String, String> queryMap);

    void followBrand(String userId, String brandId, boolean hasFollowed);
}