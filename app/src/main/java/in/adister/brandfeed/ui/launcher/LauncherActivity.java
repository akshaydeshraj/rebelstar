package in.adister.brandfeed.ui.launcher;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.HitBuilders;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import feed.data.entities.Brand;
import feed.data.entities.BrandUser;
import feed.data.entities.Post;
import in.adister.brandfeed.BrandFeedApplication;
import in.adister.brandfeed.CheckConnection;
import in.adister.brandfeed.Constants;
import in.adister.brandfeed.R;
import in.adister.brandfeed.component.AppComponent;
import in.adister.brandfeed.ui.OfferDetailsActivity;
import in.adister.brandfeed.ui.common.BaseActivity;
import in.adister.brandfeed.ui.login.LoginActivity;
import in.adister.brandfeed.ui.main.MainActivity;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;

/**
 * @author akshay
 * @version 1.0.0
 * @since 5/7/15
 */
public class LauncherActivity extends BaseActivity {

    private static final String TAG = LauncherActivity.class.getSimpleName();

    @InjectView(R.id.line1)
    TextView tv_line1;
    @InjectView(R.id.line2)
    TextView tv_line2;
    @InjectView(R.id.name)
    TextView tv_name;
    @InjectView(R.id.background)
    LinearLayout linearLayout;
    int start = 500, duration = 1000;

    @Inject
    CheckConnection checkConnection;

    @Inject
    BrandUser brandUser;

    @Inject
    BrandFeedApplication brandFeedApplication;

    int[] splashColors;
    String[] line1, line2;
    int randomColor, randomLine;
    AlphaAnimation fadeIn1, fadeIn2;

    private boolean animationFinished = false;
    private boolean branchInitialized = false;
    private boolean branchError = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        ButterKnife.inject(this);

        //Fetch the Strings / Colors.
        splashColors = getApplicationContext().getResources().getIntArray(R.array.splashscreen);
        line1 = getApplicationContext().getResources().getStringArray(R.array.line1);
        line2 = getApplicationContext().getResources().getStringArray(R.array.line2);

        //Apply FadeIn's & animate.
        fadeIn1 = new AlphaAnimation(0.0f, 1.0f);
        fadeIn2 = new AlphaAnimation(0.0f, 1.0f);

        fadeIn1.setDuration(duration);
        fadeIn1.setStartOffset(start);
        fadeIn1.setFillAfter(true);

        fadeIn2.setDuration(duration);
        fadeIn2.setStartOffset(start + duration);
        fadeIn2.setFillAfter(true);

        setUsername();
        showRandomQuote();

    }

    @Override
    protected void setupComponent(AppComponent component) {

        DaggerLauncherComponent.builder()
                .appComponent(component)
                .build()
                .inject(this);
    }

    private void setUsername() {
        if (brandUser.isLoggedIn()) {
            tv_name.setText("Hi " + brandUser.getFirstName() + " !");
        }
    }


    private Intent decideIntent() {
        if (brandUser.isLoggedIn()) {
            return new Intent(this, MainActivity.class);

        } else {
            return new Intent(this, LoginActivity.class);
        }

    }

    public void showRandomQuote() {

        //Pick a random value.
        Random random = new Random();
        randomColor = random.nextInt(splashColors.length);
        randomLine = random.nextInt(line1.length);

        //Fill the views with the colors & text.
        linearLayout.setBackgroundColor(splashColors[randomColor]);
        tv_line1.setText(line1[randomLine]);
        tv_line2.setText(line2[randomLine]);


        tv_line1.startAnimation(fadeIn1);
        tv_line2.startAnimation(fadeIn2);

        //start transition after 5 sec.
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                animationFinished = true;
                changed();

            }
        }, 2500);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName("Activity " + "Launcher");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    protected void onStart() {
        super.onStart();
        final Branch branch = Branch.getInstance();
        branch.initSession(new Branch.BranchReferralInitListener() {
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {
                if (error == null) {
                    // params are the deep linked params associated with the link that the user clicked before showing up
                    Log.i("BranchConfigTest", "deep link data: " + referringParams.toString());

                    //Need to open OfferDetailsActivity
                    if (referringParams.has("branch_post_title")) {
                        Post post = new Post();
                        try {
                            post.setImage(referringParams.getString(Constants.BRANCH_IMAGE_URL));
                            post.setLikesCount(referringParams.getString(Constants.BRANCH_POST_LIKES_COUNT));
                            post.setTitle(referringParams.getString(Constants.BRANCH_POST_TITLE));
                            post.setUpdatedAt(referringParams.getString(Constants.BRANCH_POST_TIME_AGO));
                            if (referringParams.has(Constants.BRANCH_POST_CODE)) {
                                post.setCouponCode(referringParams.getString(Constants.BRANCH_POST_CODE));
                            } else {
                                post.setCouponCode("");
                            }
                            String description = referringParams.getString(Constants.BRANCH_POST_DESCRIPTION);
                            description = description.replace("\\r\\n", "");
                            post.setContent(description);
                            post.setUrl(referringParams.getString(Constants.BRANCH_POST_URL));
                            post.setPostType(referringParams.getString(Constants.BRANCH_POST_TYPE));
                            post.setId(referringParams.getString(Constants.BRANCH_POST_ID));

                            String id = referringParams.getString(Constants.BRANCH_BRAND_ID);
                            String name = referringParams.getString(Constants.BRANCH_BRAND_NAME);
                            String logo = referringParams.getString(Constants.BRANCH_BRAND_IMAGE);
                            String categoryId = referringParams.getString(Constants.BRANCH_BRAND_CATEGORY_ID);

                            post.setBrand(id, logo, categoryId, name);

                            Brand brand = new Brand(id, name, logo, categoryId);

                            Intent intent = new Intent(LauncherActivity.this, OfferDetailsActivity.class);
                            intent.putExtra(OfferDetailsActivity.POST, post);
                            intent.putExtra(OfferDetailsActivity.BRAND, brand);
                            intent.putExtra(OfferDetailsActivity.PARAM, post.getPostType());
                            intent.putExtra(OfferDetailsActivity.VIA_LINK, true);
                            startActivity(intent);
                            finish();
                        } catch (JSONException e) {
                            Crashlytics.getInstance().core.setString("Branch.io", e.getLocalizedMessage());
                            Crashlytics.getInstance().crash();
                        }
                    } else {
                        branchInitialized = true;
                        Log.i(TAG, "branch initialized");
                        changed();
                    }
                } else {
                    branchError = true;
                    Log.i(TAG, "branch initialized");
                    changed();
                }
            }
        }, this.getIntent().getData(), this);
    }

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }

    public void changed() {
        if (animationFinished && (branchInitialized || branchError)) {

            if (!checkConnection.isConnectingToInternet()) {
                Snackbar.make(findViewById(android.R.id.content),
                        "No Internet ? Get Inspired", Snackbar.LENGTH_INDEFINITE)
                        .setAction("Click Here", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showRandomQuote();
                            }
                        })
                        .setActionTextColor(splashColors[randomColor])
                        .show();
            } else {
                startActivity(decideIntent());
                finish();
            }
        }
    }
}