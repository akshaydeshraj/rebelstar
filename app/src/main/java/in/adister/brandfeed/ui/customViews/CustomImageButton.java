package in.adister.brandfeed.ui.customViews;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import in.adister.brandfeed.R;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 04-Aug-15
 */
public class CustomImageButton extends ImageButton implements View.OnClickListener{

    private Boolean state = false;
    private String type = "like"; // Follow or like or discovery
    private Context mContext;

    private int marginTop, marginLeft;

    public CustomImageButton(Context context) {
        super(context);

        mContext = context;
    }

    public CustomImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;
        setOnClickListener(this);

        TypedArray array = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.Cib,
                0, 0);

        try {
            state = array.getBoolean(R.styleable.Cib_selected, false);
            type = array.getString(R.styleable.Cib_type);
        } finally {
            array.recycle();
        }

        setState(state);

        marginTop = Math.round(convertDpToPixel(8, mContext));
        marginLeft = Math.round(convertDpToPixel(8, mContext));
    }

    public CustomImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomImageButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width = getMeasuredWidth();
        int height;

        if(type.equals("follow")){
            if(state){
                height = (int)mContext.getResources().getDimension(R.dimen.height_follow_selected);
            }else{
                height = (int)mContext.getResources().getDimension(R.dimen.height_follow);
            }
        } else {
            height  = getMeasuredHeight();
        }

        setMeasuredDimension(width, height);
    }

    public void setState(Boolean state) {
        this.state = state;
        LinearLayout.LayoutParams linearLayoutParams;
        switch (type) {
            case "like":
                if (state) {
                    setImageResource(R.drawable.like_unselected);
                } else {
                    setImageResource(R.drawable.like_selected);
                }
                break;
            case "follow":
                int width = (int)mContext.getResources().getDimension(R.dimen.follow_width);

                if (state) {

                    setImageResource(R.drawable.ic_settings);
                    int height = (int)mContext.getResources().getDimension(R.dimen.height_follow_selected);
                    linearLayoutParams = new LinearLayout.LayoutParams(width, height);
                    linearLayoutParams.gravity = Gravity.RIGHT;
                    linearLayoutParams.setMargins(marginLeft, marginTop, marginLeft, marginTop);
                    setLayoutParams(linearLayoutParams);

                } else {

                    setImageResource(R.drawable.follow);
                    int height = (int)mContext.getResources().getDimension(R.dimen.height_follow);
                    linearLayoutParams = new LinearLayout.LayoutParams(width, height);
                    linearLayoutParams.gravity = Gravity.RIGHT;
                    linearLayoutParams.setMargins(marginLeft, marginTop, marginLeft, marginTop);
                    setLayoutParams(linearLayoutParams);

                }

                break;
            case "discovery":
                if (state) {
                    setImageResource(R.drawable.discover_enabled);
                } else {
                    setImageResource(R.drawable.discovery_disabled);
                }
                break;
        }
    }

    public boolean getState() {
        return state;
    }

    @Override
    public void onClick(View v) {
        setState(!getState());
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp      A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

}
