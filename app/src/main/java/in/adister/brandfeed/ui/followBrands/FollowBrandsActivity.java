package in.adister.brandfeed.ui.followBrands;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.analytics.HitBuilders;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import dagger.Component;
import feed.data.OnRequestFinishedListener;
import feed.data.entities.Brand;
import feed.data.entities.BrandUser;
import feed.data.entities.Category;
import feed.data.rest.RestDataSource;
import in.adister.brandfeed.ActivityScope;
import in.adister.brandfeed.Constants;
import in.adister.brandfeed.R;
import in.adister.brandfeed.adapters.FollowBrandsPagerAdapter;
import in.adister.brandfeed.component.AppComponent;
import in.adister.brandfeed.ui.common.BaseActivity;
import in.adister.brandfeed.ui.customViews.viewpagerindicator.CirclePageIndicator;
import in.adister.brandfeed.ui.main.MainActivity;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rx.subscriptions.CompositeSubscription;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 25-Aug-15
 */
public class FollowBrandsActivity extends BaseActivity implements OnRequestFinishedListener {

    @Inject
    BrandUser brandUser;

    @Inject
    RestDataSource restDataSource;

    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.follow_view_pager)
    ViewPager viewPager;
    @InjectView(R.id.indicator)
    CirclePageIndicator indicator;
    @InjectView(R.id.btn_skip)
    Button btnSkip;
    @InjectView(R.id.btn_next)
    Button btnNext;
    @InjectView(R.id.btn_done)
    Button btnDone;
    List<Category> categories = new ArrayList<>(10);
    private String parentActivity;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    private List<Brand> brandArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follow_brands);
        ButterKnife.inject(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Follow");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Add all categories to list
        categories.add(new Category(String.valueOf(15), "Entrepreneurship"));
        categories.add(new Category(String.valueOf(14), "E-Commerce"));
        categories.add(new Category(String.valueOf(17), "Fashion & Beauty"));
        categories.add(new Category(String.valueOf(13), "Food"));
        categories.add(new Category(String.valueOf(16), "Travel"));
        categories.add(new Category(String.valueOf(4), "Services"));
        categories.add(new Category(String.valueOf(7), "News"));
        categories.add(new Category(String.valueOf(8), "Fun"));
        categories.add(new Category(String.valueOf(9), "Miscellaneous"));

        parentActivity = getIntent().getStringExtra(Constants.PARENT_ACTIVITY);

        restDataSource.setOnRequestFinishedListener(this);

        restDataSource.getBrandList();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mTracker.setScreenName("Activity " + "FollowBrands");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void setupComponent(AppComponent component) {

        DaggerFollowBrandsActivity_FollowBrandsComponent.builder()
                .appComponent(component)
                .build()
                .inject(this);
    }

    @Override
    public void onSuccess(Object response, Response retrofitResponse) {

        brandArrayList = (List<Brand>) response;
        setupViewPager();
    }

    @Override
    public void onError(RetrofitError error) {

    }

    public void setupViewPager() {

        viewPager.setOffscreenPageLimit(8);

        int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                20 * 2, getResources().getDisplayMetrics());
        viewPager.setPageMargin(-margin);

        final FollowBrandsPagerAdapter adapter = new FollowBrandsPagerAdapter(getSupportFragmentManager());

        for (int i = 0; i < categories.size(); i++) {
            adapter.add(CategoryFragment.newInstance(brandNameFromCategoryId(
                    categories.get(i).getCategoryId()), brandIdFromCategoryId(
                    categories.get(i).getCategoryId()), categories.get(i).getCategoryName()));
        }

        viewPager.setAdapter(adapter);

        indicator.setViewPager(viewPager);
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 8) {
                    btnDone.setVisibility(View.VISIBLE);
                    btnSkip.setVisibility(View.GONE);
                    btnNext.setVisibility(View.GONE);
                } else {
                    btnDone.setVisibility(View.GONE);
                    btnSkip.setVisibility(View.VISIBLE);
                    btnNext.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @OnClick(R.id.btn_skip)
    public void skipFollow() {

        if (parentActivity.equals("LoginActivity")) {
            Intent intent = new Intent(FollowBrandsActivity.this, MainActivity.class);
            startActivity(intent);
        }
        finish();
    }

    @OnClick(R.id.btn_next)
    public void nextCategory() {
        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
    }

    @OnClick(R.id.btn_done)
    public void doneFollow() {
        if (parentActivity.equals("LoginActivity")) {
            Intent intent = new Intent(FollowBrandsActivity.this, MainActivity.class);
            startActivity(intent);
        }
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        compositeSubscription.unsubscribe();
    }

    private void showProgress() {

    }

    private void hideProgress() {

    }

    private ArrayList<String> brandNameFromCategoryId(String categoryId) {

        List<Brand> allBrandsList = brandArrayList;
        ArrayList<String> brandsArrayList = new ArrayList<>();

        for (Brand brand : allBrandsList) {
            if (brand.getCategoryId().trim().equals(categoryId)) {
                brandsArrayList.add(brand.getName());
            }
        }

        return brandsArrayList;
    }

    private ArrayList<String> brandIdFromCategoryId(String categoryId) {

        List<Brand> allBrandsList = brandArrayList;
        ArrayList<String> brandsArrayList = new ArrayList<>();

        for (Brand brand : allBrandsList) {
            if (brand.getCategoryId().trim().equals(categoryId)) {
                brandsArrayList.add(brand.getId());
            }
        }

        return brandsArrayList;
    }


    @ActivityScope
    @Component(
            dependencies = AppComponent.class
    )
    public interface FollowBrandsComponent {

        void inject(FollowBrandsActivity activity);
    }

    public CompositeSubscription getCompositeSubscription() {
        return compositeSubscription;
    }

    public BrandUser getBrandUser() {
        return brandUser;
    }
}