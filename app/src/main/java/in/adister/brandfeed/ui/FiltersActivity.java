package in.adister.brandfeed.ui;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.analytics.HitBuilders;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectViews;
import butterknife.OnClick;
import feed.data.entities.FilterModel;
import in.adister.brandfeed.R;
import in.adister.brandfeed.component.AppComponent;
import in.adister.brandfeed.ui.common.BaseActivity;
import in.adister.brandfeed.ui.customViews.SelectableTextView;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 23-Aug-15
 */
public class FiltersActivity extends BaseActivity {

    @InjectViews({R.id.tv_cat_fashion, R.id.tv_cat_personalities, R.id.tv_cat_brands,
            R.id.tv_cat_fun, R.id.tv_cat_news, R.id.tv_cat_organizations, R.id.tv_cat_startups})
    List<SelectableTextView> categories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filters);
        ButterKnife.inject(this);

        //Set states of all categories
        int i=0;
        for(SelectableTextView textView : categories){
            if(FilterModel.getInstance().getCategoryPositions().contains(String.valueOf(i))){
                textView.setTVSelected(true);
            }else{
                textView.setTVSelected(false);
            }
            i++;
        }

    }

    @Override
    protected void setupComponent(AppComponent component) {

    }

    @Override
    protected void onResume() {
        super.onResume();

        mTracker.setScreenName("Activity " + "Filters");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @OnClick(R.id.tvSelectAllCategories)
    public void selectAllCategories() {

        for (SelectableTextView textView : categories) {
            textView.setTVSelected(true);
        }
    }

    @OnClick(R.id.ib_back)
    public void finishActivity() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", "From FiltersActivity");
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    @OnClick(R.id.ib_reset)
    public void reset() {
        FilterModel.getInstance().reset();

        //Set state of all categories false
        for(SelectableTextView textView : categories){
            textView.setTVSelected(false);
        }
    }

    @OnClick(R.id.ib_done)
    public void applyFilter() {

        //Reset if there are any existing values
        FilterModel.getInstance().reset();

        //List of category id's
        for (int i = 0; i < categories.size(); i++) {
            if (categories.get(i).getTVSelected()) {
                FilterModel.getInstance().addElement(String.valueOf(i));
            }
        }

        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", "From FiltersActivity");
        setResult(RESULT_OK, returnIntent);
        finish();
    }
}