package in.adister.brandfeed.ui.login;

import feed.data.entities.BrandUser;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 08-Aug-15
 */
public interface LoginView {

    void showProgress();

    void hideProgress();

    void setSuccess(BrandUser response, Response retrofitResponse);

    void setError(RetrofitError error);
}
