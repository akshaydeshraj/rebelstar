package in.adister.brandfeed.ui.common;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.analytics.Tracker;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import in.adister.brandfeed.BrandFeedApplication;
import in.adister.brandfeed.component.AppComponent;

/**
 * Created by zopper on 7/6/15.
 */
public abstract class BaseActivity extends AppCompatActivity {

    public static final String MIXPANEL_TOKEN = "121cbb8c2cee2bd2692c7c0bbec91e39";

    public MixpanelAPI mixpanelAPI;

    public Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupComponent(BrandFeedApplication.get(this).component());

        mixpanelAPI = MixpanelAPI.getInstance(this, MIXPANEL_TOKEN);

        BrandFeedApplication app = (BrandFeedApplication) getApplication();
        mTracker = app.getDefaultTracker();
    }

    protected abstract void setupComponent(AppComponent component);

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mixpanelAPI.flush();
    }

    public MixpanelAPI getMixpanelAPI() {
        return mixpanelAPI;
    }

}
