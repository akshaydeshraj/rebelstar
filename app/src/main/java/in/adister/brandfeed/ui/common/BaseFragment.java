package in.adister.brandfeed.ui.common;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.google.android.gms.analytics.Tracker;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import in.adister.brandfeed.BrandFeedApplication;
import in.adister.brandfeed.component.AppComponent;

/**
 * Created by zopper on 13/6/15.
 */
public abstract class BaseFragment extends Fragment {

    public static final String MIXPANEL_TOKEN = "121cbb8c2cee2bd2692c7c0bbec91e39";

    public MixpanelAPI mixpanelAPI;

    public Tracker mTracker;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupComponent(BrandFeedApplication.get(getActivity()).component());

        mixpanelAPI = MixpanelAPI.getInstance(getActivity(), MIXPANEL_TOKEN);

        BrandFeedApplication app = (BrandFeedApplication) getActivity().getApplication();
        mTracker = app.getDefaultTracker();
    }

    protected abstract void setupComponent(AppComponent appComponent);
}
