package in.adister.brandfeed.ui.tabs;

import java.util.List;
import java.util.Map;

import feed.data.OnRequestFinishedListener;
import feed.data.entities.Post;
import feed.data.rest.RestDataSource;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by zopper on 13/6/15.
 */
public class TabsPresenterImpl implements TabsPresenter, OnRequestFinishedListener {

    private TabsView tabsView;
    private RestDataSource restDataSource;

    public TabsPresenterImpl(TabsView tabsView, RestDataSource restDataSource) {
        this.tabsView = tabsView;
        this.restDataSource = restDataSource;
        restDataSource.setOnRequestFinishedListener(this);
    }

    @Override
    public void init(String param, Map<String, String> queryMap) {
        tabsView.showProgress();
        restDataSource.fetchData(param,queryMap);
    }

    @Override
    public void followBrand(String userId, String brandId, boolean hasFollowed) {
        tabsView.showProgress();
        restDataSource.followBrand(userId, brandId, hasFollowed)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Response response) {
                tabsView.setSuccessLayout(response);
            }
        });
    }

    @Override
    public void onSuccess(Object response, Response retrofitResponse) {

        /*if(response instanceof ItemsResponse){
            tabsView.hideProgress();
            tabsView.setSuccessLayout((ItemsResponse) response, retrofitResponse);
        }*/
        tabsView.hideProgress();
        if (response instanceof List) {

            tabsView.setSuccessLayout((List<Post>) response, retrofitResponse);
        } else {
            tabsView.setSuccessLayout(retrofitResponse);
        }


    }

    @Override
    public void onError(RetrofitError retrofitError) {
        tabsView.hideProgress();
        tabsView.setErrorLayout(retrofitError);
    }
}