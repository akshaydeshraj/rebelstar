package in.adister.brandfeed.ui.followBrands;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.gson.Gson;

import org.apmem.tools.layouts.FlowLayout;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import dagger.Component;
import feed.data.entities.BrandUser;
import feed.data.rest.RestDataSource;
import in.adister.brandfeed.ActivityScope;
import in.adister.brandfeed.Constants;
import in.adister.brandfeed.R;
import in.adister.brandfeed.component.AppComponent;
import in.adister.brandfeed.ui.common.BaseFragment;
import in.adister.brandfeed.ui.customViews.SelectableTextView;
import retrofit.client.Response;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 25-Aug-15
 */
public class CategoryFragment extends BaseFragment implements View.OnClickListener {

    private static final String TAG = CategoryFragment.class.getSimpleName();

    public static final String CAT_NAME = "categoryName";
    private static final String BRAND_LIST = "brandList";
    private static final String BRAND_ID_LIST = "brandIdList";

    @ActivityScope
    @Component(
            dependencies = AppComponent.class
    )
    public interface CategoryComponent {

        void inject(CategoryFragment categoryFragment);
    }

    @Inject
    RestDataSource restDataSource;

    @Inject
    SharedPreferences preferences;
    @Inject
    SharedPreferences.Editor editor;

    @InjectView(R.id.tv_cat_title)
    TextView tvTitle;
    @InjectView(R.id.rl_brand_holder)
    FlowLayout llBrandHolder;

    BrandUser brandUser;

    private List<String> brandList;
    private List<String> brandIdList;
    private String categoryName;

    CompositeSubscription compositeSubscription;

    public static CategoryFragment newInstance(ArrayList<String> brandList,
                                               ArrayList<String> brandIdList, String categoryName) {

        CategoryFragment categoryFragment = new CategoryFragment();

        Bundle args = new Bundle();
        args.putStringArrayList(BRAND_LIST, brandList);
        args.putStringArrayList(BRAND_ID_LIST, brandIdList);
        args.putString(CAT_NAME, categoryName);
        categoryFragment.setArguments(args);
        return categoryFragment;
    }

    @Override
    protected void setupComponent(AppComponent appComponent) {
        DaggerCategoryFragment_CategoryComponent.builder()
                .appComponent(appComponent)
                .build()
                .inject(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        brandList = getArguments() != null ? getArguments().getStringArrayList(BRAND_LIST) : new ArrayList<String>();
        brandIdList = getArguments() != null ? getArguments().getStringArrayList(BRAND_ID_LIST) : new ArrayList<String>();
        categoryName = getArguments().getString(CAT_NAME);

        compositeSubscription = ((FollowBrandsActivity)getActivity()).getCompositeSubscription();

        brandUser = ((FollowBrandsActivity)getActivity()).getBrandUser();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_follow_brand, container, false);

        ButterKnife.inject(this, rootView);

        tvTitle.setText(categoryName);

        int i = 0;

        for (String string : brandList) {

            SelectableTextView textView = new SelectableTextView(getActivity());

            textView.setType("brand");
            textView.setText(string + "    ");

            if (brandUser.contains(brandIdList.get(i))) {
                textView.setTVSelected(true);
            } else {
                textView.setTVSelected(false);
            }

            textView.setTag(brandIdList.get(i));
            i++;
            textView.setOnClickListener(this);

            llBrandHolder.addView(textView);

            new MaterialShowcaseView.Builder(getActivity())
                    .setTarget(textView)
                    .setDismissText("GOT IT")
                    .setContentText("Follow your favourite startups. Swipe right to see more categories.")
                    .singleUse("text")
                    .setDismissOnTouch(true)
                    .show();
        }

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("CategoryFragment " + categoryName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onClick(View v) {

        if (v instanceof SelectableTextView) {
            boolean selected = ((SelectableTextView) v).getTVSelected();

             Subscription subscription = restDataSource.followBrand(brandUser.getId(), v.getTag().toString(),
                     !selected)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response>() {
                        @Override
                        public void onCompleted() {
                            Log.i(TAG, "Request Completed");
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.i(TAG, "Request Error : "+e.getMessage());
                        }

                        @Override
                        public void onNext(Response response) {
                            Log.i(TAG, "Request onNext");
                        }
                    });

            compositeSubscription.add(subscription);

            ((SelectableTextView) v).setTVSelected(!selected);

            if (!selected) {
                brandUser.addBrand(v.getTag().toString());
                Log.v("CategoryFragment", v.getTag().toString());
            } else {
                brandUser.removeBrand(v.getTag().toString());
                Log.v("CategoryFragment", v.getTag().toString());
            }

            String json = new Gson().toJson(brandUser);
            editor.putString(Constants.USER_BRAND, json);
            boolean completed = editor.commit();
        }
    }
}