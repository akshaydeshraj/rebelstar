package in.adister.brandfeed.ui.common;

import android.content.res.TypedArray;
import android.util.TypedValue;

import in.adister.brandfeed.R;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 18-Sep-15
 */
public abstract class BaseScrollActivity extends BaseActivity {

    protected int getActionBarSize() {
        TypedValue typedValue = new TypedValue();
        int[] textSizeAttr = new int[]{R.attr.actionBarSize};
        int indexOfAttrTextSize = 0;
        TypedArray a = obtainStyledAttributes(typedValue.data, textSizeAttr);
        int actionBarSize = a.getDimensionPixelSize(indexOfAttrTextSize, -1);
        a.recycle();
        return actionBarSize;
    }

    protected int getScreenHeight() {
        return findViewById(android.R.id.content).getHeight();
    }

}
