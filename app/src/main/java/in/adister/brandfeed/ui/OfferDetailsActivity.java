package in.adister.brandfeed.ui;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.nineoldandroids.view.ViewHelper;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import dagger.Component;
import feed.data.OnRequestFinishedListener;
import feed.data.entities.BrandUser;
import feed.data.entities.Post;
import feed.data.rest.RestDataSource;
import in.adister.brandfeed.ActivityScope;
import in.adister.brandfeed.BuildConfig;
import in.adister.brandfeed.Constants;
import in.adister.brandfeed.DateFormatter;
import in.adister.brandfeed.R;
import in.adister.brandfeed.component.AppComponent;
import in.adister.brandfeed.ui.common.BaseActivity;
import in.adister.brandfeed.ui.common.CustomLinkMovementMethod;
import in.adister.brandfeed.ui.common.CustomTagHandler;
import in.adister.brandfeed.ui.common.ImageGetter;
import in.adister.brandfeed.ui.customViews.CustomImageButton;
import in.adister.brandfeed.ui.customtabs.CustomTabActivityHelper;
import in.adister.brandfeed.ui.customtabs.WebviewFallback;
import in.adister.brandfeed.ui.main.MainActivity;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.BranchShortLinkBuilder;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 18-Sep-15
 */
public class OfferDetailsActivity extends BaseActivity implements ObservableScrollViewCallbacks, OnRequestFinishedListener {

    public static final String TAG = OfferDetailsActivity.class.getSimpleName();

    //Keys For Intent
    public static final String PARAM = "param";
    public static final String POST = "post";
    public static final String BRAND = "brand";
    public static final String VIA_LINK = "via_link";

    private boolean viaLink;

    @Inject
    Picasso picasso;
    @Inject
    BrandUser brandUser;
    @Inject
    RestDataSource restDataSource;
    @InjectView(R.id.iv_coupon_image)
    ImageView mImageView;
    @InjectView(R.id.overlay)
    View mOverlayView;
    @InjectView(R.id.title)
    TextView mTitleView;
    @InjectView(R.id.scroll)
    ObservableScrollView mScrollView;
    @InjectView(R.id.image_block)
    View imageBlock;
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.cib_like)
    CustomImageButton mFab;
    @InjectView(R.id.rl_scroll)
    RelativeLayout relativeLayout;
    @InjectView(R.id.iv_brand_pic)
    ImageView brandPic;
    @InjectView(R.id.tv_brand_name)
    TextView tvBrandName;
    @InjectView(R.id.tv_post_time)
    TextView tvPostTime;
    @InjectView(R.id.tv_no_likes)
    TextView tvPostLikeCount;
    @InjectView(R.id.cib_follow)
    CustomImageButton cibFollow;
    @InjectView(R.id.tv_post_title)
    TextView tvPostTitle;
    @InjectView(R.id.tv_description)
    TextView tvPostDescription;
    @InjectView(R.id.tv_coupon_code)
    TextView tvPostCode;
    @InjectView(R.id.tv_coupon_share)
    TextView tvPostShare;
    @InjectView(R.id.tv_coupon_report)
    TextView tvPostReport;

    CardView cardView;

    Post post;

    private String urlToPost = "";
    private String postType = "";
    private int numberOfLikes = 0;

    private JSONObject props;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_details);
        ButterKnife.inject(this);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("");
        }

        cardView = (CardView) findViewById(R.id.card_code);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");

        //set typeface to all textviews
        tvBrandName.setTypeface(typeface);
        tvPostTime.setTypeface(typeface);
        tvPostLikeCount.setTypeface(typeface);
        tvPostTitle.setTypeface(typeface);
        tvPostDescription.setTypeface(typeface);
        tvPostCode.setTypeface(typeface);
        tvPostShare.setTypeface(typeface);
        tvPostReport.setTypeface(typeface);

        mScrollView.setScrollViewCallbacks(this);

        ScrollUtils.addOnGlobalLayoutListener(mScrollView, new Runnable() {
            @Override
            public void run() {
                mScrollView.scrollTo(0, 1);
            }
        });

        restDataSource.setOnRequestFinishedListener(this);

        //Data to be sent with every event
        props = new JSONObject();
        try {
            props.put("AppVersion", BuildConfig.VERSION_NAME);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Intent intent = getIntent();

        post = (Post) intent.getSerializableExtra(POST);

        viaLink = intent.getBooleanExtra(VIA_LINK, false);

        loadBackdrop(post.getImage());
        urlToPost = post.getUrl();

        Log.i("BugFollowedActivity", String.valueOf(post.getBrand().getId())+" "+brandUser.contains(post.getBrand().getId()));
        if (brandUser.contains(post.getBrand().getId())) {
            cibFollow.setState(true);
        } else {
            cibFollow.setState(false);
        }

        picasso.load(BuildConfig.IMAGE_BASE_URL + post.getBrand().getLogo()).
                fit().centerCrop().into(brandPic, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError() {
                picasso.load(post.getBrand().getLogo())
                        .fit().centerCrop().into(brandPic);
            }
        });

        tvPostTitle.setText(post.getTitle());

        tvPostDescription.setMovementMethod(new CustomLinkMovementMethod() {
            @Override
            public void onLinkClick(String url) {
                CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder()
                        .setToolbarColor(ContextCompat.getColor(
                                OfferDetailsActivity.this, R.color.colorPrimary))
                        .setCloseButtonIcon(BitmapFactory.
                                decodeResource(OfferDetailsActivity.this.getResources(),
                                        R.drawable.ic_arrow_back_white_24dp))
                        .build();
                CustomTabActivityHelper.openCustomTab(
                        OfferDetailsActivity.this, customTabsIntent, Uri.parse(url), new WebviewFallback());

            }
        });
        tvPostDescription.setLinkTextColor(Color.BLUE);
        tvPostDescription.setText(Html.fromHtml(post.getContent() != null ? post.getContent() : post.getShortDescription(),
                new ImageGetter(getResources(), picasso, tvPostDescription), new CustomTagHandler()));

        tvPostTime.setText(DateFormatter.getTimeAgo(post.getUpdatedAt()));

        tvBrandName.setText(post.getBrand().getName());

        postType = intent.getStringExtra(OfferDetailsActivity.PARAM);

        if (post.getPostType().equals("story")) {
            Button button = (Button) findViewById(R.id.ib_post_buy);
            button.setText("READ");
            cardView.setVisibility(View.GONE);
            //  priceTime.setVisibility(View.GONE);
        }

        if (!post.getPostType().equals("story")) {
            if (post.getCouponCode() == null || "".equals(post.getCouponCode())) {
                tvPostCode.setText("CODE : Not Required");
            } else {
                tvPostCode.setText("CODE : " + post.getCouponCode());
            }
        }


        try {
            numberOfLikes = Integer.parseInt(post.getLikesCount());
            tvPostLikeCount.setText(numberOfLikes == 1 ? numberOfLikes + " star" : numberOfLikes + " stars");
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (post.isUserLikesThisPost()) {
            mFab.setState(true);
        } else {
            mFab.setState(false);
        }

        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mFab.getState()) {
                    mFab.setState(false);
                    numberOfLikes--;
                } else {
                    mFab.setState(true);
                    numberOfLikes++;
                }

                tvPostLikeCount.setText(numberOfLikes == 1 ? numberOfLikes + " star" : numberOfLikes + " stars");

                restDataSource.likePost(brandUser.getId(), post.getId(), postType, mFab.getState());

            }

        });

        tvPostShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i(TAG, "Share Button Clicked");
                createShortLink();
            }
        });

        tvPostReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(findViewById(android.R.id.content), "Thanks for your feedback", Snackbar.LENGTH_SHORT)
                        .show();
            }
        });

        tvPostCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(post.getCouponCode() == null || "".equals(post.getCouponCode()))) {
                    android.content.ClipboardManager clipboard =
                            (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    android.content.ClipData clip =
                            android.content.ClipData.newPlainText("Copied Text", post.getCouponCode());
                    clipboard.setPrimaryClip(clip);

                    Toast.makeText(OfferDetailsActivity.this, "Coupon code copied", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void share(String url) {
        String shareText = "Check out \"" + post.getTitle() + " - " + post.getBrand().getName() + "\" @ " + url;
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, shareText);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    public void createShortLink() {
        BranchShortLinkBuilder shortUrlBuilder = new BranchShortLinkBuilder(this)
                .addParameters(Constants.BRANCH_IMAGE_URL, post.getImage())
                .addParameters(Constants.BRANCH_BRAND_IMAGE, post.getBrand().getLogo())
                .addParameters(Constants.BRANCH_BRAND_ID, post.getBrand().getId())
                .addParameters(Constants.BRANCH_BRAND_CATEGORY_ID, post.getBrand().getCategoryId())
                .addParameters(Constants.BRANCH_BRAND_NAME, post.getBrand().getName())
                .addParameters(Constants.BRANCH_POST_TITLE, post.getTitle())
                .addParameters(Constants.BRANCH_POST_CODE, post.getCouponCode() == null ? "" : post.getCouponCode())
                .addParameters(Constants.BRANCH_POST_DESCRIPTION, post.getContent())
                .addParameters(Constants.BRANCH_POST_TIME_AGO, post.getUpdatedAt())
                .addParameters(Constants.BRANCH_POST_URL, post.getUrl())
                .addParameters(Constants.BRANCH_POST_LIKES_COUNT, post.getLikesCount())
                .addParameters(Constants.BRANCH_POST_ID, post.getId())
                .addParameters(Constants.BRANCH_POST_TYPE, post.getPostType());

        shortUrlBuilder.addParameters(Constants.SHARE_USER_ID, brandUser.getId());

        shortUrlBuilder.generateShortUrl(new Branch.BranchLinkCreateListener() {
            @Override
            public void onLinkCreate(String url, BranchError error) {
                if (error != null) {
                    Log.e(TAG, "Branch create short url failed. Caused by -" + error.getMessage());
                } else {
                    share(url);
                }
            }
        });
    }

    @Override
    protected void setupComponent(AppComponent component) {

        DaggerOfferDetailsActivity_OfferDetailsComponent.builder()
                .appComponent(component)
                .build()
                .inject(this);
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {

        ViewHelper.setTranslationY(imageBlock, scrollY / 3);
    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onBackPressed() {
        setResultIntent();
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }

    @OnClick(R.id.cib_follow)
    public void followBrand(final CustomImageButton customImageButton) {

        if (customImageButton.getState()) {

            //show a popup menu
            PopupMenu popupMenu = new PopupMenu(OfferDetailsActivity.this, customImageButton);
            popupMenu.getMenuInflater().inflate(R.menu.details_popup_menu, popupMenu.getMenu());

            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.menu_offer_unfollow:
                            customImageButton.setState(!customImageButton.getState());
                            restDataSource.followBrand(brandUser.getId(), post.getBrand().getId(), customImageButton.getState());
                            break;
                    }
                    return true;
                }
            });

            popupMenu.show();

        } else {
            customImageButton.setState(!customImageButton.getState());
            restDataSource.followBrand(brandUser.getId(), post.getBrand().getId(), customImageButton.getState());
        }

    }

    @OnClick(R.id.ib_post_buy)
    public void openWebViewActivity() {

        CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder()
                .setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary))
                .setCloseButtonIcon(BitmapFactory.decodeResource(this.getResources(),
                        R.drawable.ic_arrow_back_white_24dp))
                .build();
        CustomTabActivityHelper.openCustomTab(
                this, customTabsIntent, Uri.parse(urlToPost), new WebviewFallback());

        if (!(post.getCouponCode() == null || "".equals(post.getCouponCode()))) {
            android.content.ClipboardManager clipboard =
                    (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip =
                    android.content.ClipData.newPlainText("Copied Text", post.getCouponCode());
            clipboard.setPrimaryClip(clip);

            Toast.makeText(this, "Coupon code copied", Toast.LENGTH_SHORT).show();

        }

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentName("WebViewActivity")
                .putContentId(post.getId())
                .putContentType(post.getPostType())
                .putCustomAttribute("BrandId", post.getBrandId())
                .putCustomAttribute("BrandId", post.getBrand().getName()));

        try {
            props.put("postType", post.getPostType());
            props.put("postMrp", post.getMrp());
            props.put("brandId", post.getBrand().getId());
            props.put("interest", post.getBrand().getCategoryId());
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        mixpanelAPI.track("buy", props);
    }

    protected int getActionBarSize() {
        TypedValue typedValue = new TypedValue();
        int[] textSizeAttr = new int[]{R.attr.actionBarSize};
        int indexOfAttrTextSize = 0;
        TypedArray a = obtainStyledAttributes(typedValue.data, textSizeAttr);
        int actionBarSize = a.getDimensionPixelSize(indexOfAttrTextSize, -1);
        a.recycle();
        return actionBarSize;
    }

    private void loadBackdrop(final String url) {

        try {
            picasso.load(url).fit().centerCrop().
                    into(mImageView, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                            picasso.load(R.drawable.rebel_cover).into(mImageView);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
            picasso.load(R.drawable.rebel_cover).into(mImageView);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResultIntent();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSuccess(Object response, Response retrofitResponse) {

    }

    @Override
    public void onError(RetrofitError error) {

    }

    public void setResultIntent() {

        Intent intent;
        if (viaLink) {
            intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            this.finish();
        } else {
            intent = new Intent();
            intent.putExtra("userLiked", mFab.getState());
            intent.putExtra("likesCount", numberOfLikes);
            intent.putExtra("isFollowed", cibFollow.getState());
            Log.v(TAG, String.valueOf(numberOfLikes));
            setResult(RESULT_OK, intent);
            this.finish();
        }

    }


    @ActivityScope
    @Component(
            dependencies = AppComponent.class
    )
    public interface OfferDetailsComponent {

        void inject(OfferDetailsActivity activity);
    }
}