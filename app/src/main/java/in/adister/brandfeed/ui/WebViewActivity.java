package in.adister.brandfeed.ui;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import butterknife.ButterKnife;
import butterknife.InjectView;
import in.adister.brandfeed.R;

/**
 * Created by zopper on 13/6/15.
 */
public class WebViewActivity extends AppCompatActivity {

    @InjectView(R.id.webview)
    WebView mWebView;

    @InjectView(R.id.progressBar)
    ProgressBar mProgressBar;

    @InjectView(R.id.app_bar)
    Toolbar mToolBar;

    private int overrideCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        ButterKnife.inject(this);

        overridePendingTransition(R.anim.slide_in_up, R.anim.fade_out);

        setSupportActionBar(mToolBar);
        mToolBar.setBackgroundColor(Color.parseColor("#ffffff"));

        mWebView.getSettings().setBuiltInZoomControls(true);

        mWebView.setWebViewClient(new BrandFeedWebViewClient());
        mWebView.setWebChromeClient(new BrandFeedWebChromeClient());

        mProgressBar.setMax(100);

        mWebView.getSettings().setJavaScriptEnabled(true);

        String url = getIntent().getStringExtra("url");
        mWebView.loadUrl(url);

        setProgress(0);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.slide_out_up);
    }

    public void setValue(int progress) {
        this.mProgressBar.setProgress(progress);
    }

    private class BrandFeedWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.v("URL", url);
            if(url.startsWith("http")){
                view.loadUrl(url);
                Log.v("URL-http", url);
            }else{
                if(overrideCount == 0){
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    startActivity(intent);
                    WebViewActivity.this.finish();
                    overrideCount++;
                }
            }
            return false;
        }
    }

    private class BrandFeedWebChromeClient extends WebChromeClient {

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            WebViewActivity.this.setValue(newProgress);
            super.onProgressChanged(view, newProgress);
        }

        @Override
        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
            mToolBar.setTitleTextColor(Color.parseColor("#000000"));
            mToolBar.setTitle(title);
        }
    }
}