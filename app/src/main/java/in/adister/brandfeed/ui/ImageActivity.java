package in.adister.brandfeed.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import in.adister.brandfeed.BuildConfig;
import in.adister.brandfeed.Constants;
import in.adister.brandfeed.R;

/**
 * Created by Yogendra Singh on 9/3/2015.
 */
public class ImageActivity extends AppCompatActivity {

  @InjectView(R.id.image)
  ImageView image;

  @InjectView(R.id.toolbar)
  Toolbar toolbar;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_image);
    ButterKnife.inject(this);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    final String imageUrl = getIntent().getExtras().getString(Constants.IMAGE);

    Picasso.with(this).load(imageUrl).
        fit().centerCrop().into(image, new Callback() {
      @Override
      public void onSuccess() {

      }

      @Override
      public void onError() {
        Picasso.with(ImageActivity.this).load(BuildConfig.IMAGE_BASE_URL + imageUrl)
            .fit().centerCrop().into(image);
      }
    });
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }
}
