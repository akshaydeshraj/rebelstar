package in.adister.brandfeed.ui.tabs;

import java.util.List;

import feed.data.entities.Post;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by zopper on 13/6/15.
 */
public interface TabsView {

    void showProgress();

    void hideProgress();

    void setSuccessLayout(List<Post> itemsResponse, Response retrofitResponse);

    void setSuccessLayout(Response retrofitResponse);

    void setErrorLayout(RetrofitError retrofitError);


}
