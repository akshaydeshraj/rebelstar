package in.adister.brandfeed.ui.customtabs;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import in.adister.brandfeed.ui.WebViewActivity;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 14-Oct-15
 */
public class WebviewFallback implements CustomTabActivityHelper.CustomTabFallback {

    @Override
    public void openUri(Activity activity, Uri uri) {
        Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra("url", uri.toString());
        activity.startActivity(intent);
    }
}
