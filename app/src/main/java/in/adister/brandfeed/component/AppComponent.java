package in.adister.brandfeed.component;

import android.content.SharedPreferences;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;
import feed.data.entities.BrandUser;
import feed.data.rest.RestDataSource;
import in.adister.brandfeed.BrandFeedApplication;
import in.adister.brandfeed.CheckConnection;
import in.adister.brandfeed.modules.ApiModule;
import in.adister.brandfeed.modules.AppModule;
import retrofit.Endpoint;
import retrofit.client.Client;

/**
 * Created by zopper on 7/6/15.
 */

@Singleton
@Component(
        modules = {
                AppModule.class,
                ApiModule.class
        }
)
public interface AppComponent {

    void inject(BrandFeedApplication application);

    BrandFeedApplication getApplication();

    Endpoint getEndPoint();

    OkHttpClient getOkHttpClient();

    Client getClient();

    RestDataSource getRestDataSource();

    SharedPreferences getDefaultSharedPreferances();

    SharedPreferences.Editor getSharedPrefsEdior();

    CheckConnection getCheckConnection();

    Picasso getPicasso();

    BrandUser getBrandUser();
}