package in.adister.brandfeed.component;

import dagger.Component;
import in.adister.brandfeed.ActivityScope;
import in.adister.brandfeed.modules.MainModule;
import in.adister.brandfeed.ui.main.MainActivity;

/**
 * Created by zopper on 7/6/15.
 */

@ActivityScope
@Component(
        dependencies = AppComponent.class,
        modules = MainModule.class
)
public interface MainComponent {

    void inject(MainActivity activity);

}
