package in.adister.brandfeed.component;

import dagger.Component;
import in.adister.brandfeed.ActivityScope;
import in.adister.brandfeed.modules.LoginModule;
import in.adister.brandfeed.ui.login.LoginActivity;
import in.adister.brandfeed.ui.login.LoginPresenter;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 08-Aug-15
 */

@ActivityScope
@Component(
        dependencies = AppComponent.class,
        modules = {
                LoginModule.class
        }
)
public interface LoginComponent {

    void inject(LoginActivity activity);

    LoginPresenter getLoginPresenter();
}