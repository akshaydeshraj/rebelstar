package in.adister.brandfeed.component;

import dagger.Component;
import in.adister.brandfeed.ActivityScope;
import in.adister.brandfeed.modules.TabsModule;
import in.adister.brandfeed.ui.tabs.TabsFragment;
import in.adister.brandfeed.ui.tabs.TabsPresenter;

/**
 * Created by zopper on 13/6/15.
 */

@ActivityScope
@Component(
        dependencies = AppComponent.class,
        modules = {
                TabsModule.class
        }
)
public interface TabsComponent {

    void inject(TabsFragment tabsFragment);

    TabsPresenter getTabsPresenter();
}
