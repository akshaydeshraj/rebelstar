package in.adister.brandfeed.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 25-Aug-15
 */
public class FollowBrandsPagerAdapter extends FragmentStatePagerAdapter {

    private final List<Fragment> mFragments = new ArrayList<>();

    public FollowBrandsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void add(Fragment fragment){
        mFragments.add(fragment);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

/*    @Override
    public float getPageWidth(int position) {
        //return super.getPageWidth(position);
        return 0.8f;
    }*/
}
