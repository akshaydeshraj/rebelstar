package in.adister.brandfeed.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import feed.data.entities.BrandUser;
import feed.data.entities.Post;
import in.adister.brandfeed.BuildConfig;
import in.adister.brandfeed.Constants;
import in.adister.brandfeed.DateFormatter;
import in.adister.brandfeed.R;
import in.adister.brandfeed.ui.BrandActivity;
import in.adister.brandfeed.ui.customViews.CustomImageButton;
import in.adister.brandfeed.ui.customtabs.CustomTabActivityHelper;
import in.adister.brandfeed.ui.customtabs.WebviewFallback;
import in.adister.brandfeed.ui.tabs.TabsFragment;

/**
 * Created by zopper on 13/6/15.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private List<Post> items;
    private Context mContext;
    private Picasso picasso;
    private String param;

    private Fragment fragment;
    private Activity activity;

    private BrandUser brandUser;

    private Typeface typeface;

    private String source = "";

    public RecyclerViewAdapter(Context mContext, Picasso picasso, BrandUser brandUser) {

        this.mContext = mContext;
        this.picasso = picasso;
        this.brandUser = brandUser;

        try {
            typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/Roboto-Medium.ttf");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public List<Post> getItems() {
        return items;
    }

    public void setItems(List<Post> items) {
        this.items = items;
/*
        Collections.sort(items);
        Collections.reverse(items);*/
    }

    public void setParameter(String param) {
        this.param = param;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate
                (R.layout.fragment_single_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final Post values = items.get(position);

        //Setup Crashlytics keys
        Crashlytics.getInstance().core.setInt("position", position);
        Crashlytics.getInstance().core.setString("param", param);
        Crashlytics.getInstance().core.setString("brand", values.getBrandId());
        Crashlytics.getInstance().core.setString("title", values.getTitle());

        //Set state of follow button in adapter if the user is logged in
        if (brandUser.contains(values.getBrandId())) {
            holder.followBrand.setState(true);
        } else {
            holder.followBrand.setState(false);
        }

        picasso.load(BuildConfig.IMAGE_BASE_URL + values.getBrand().getLogo())
                .fit().centerCrop().into(holder.brandPic, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError() {
                picasso.load(values.getBrand().getLogo())
                        .fit().centerCrop().into(holder.brandPic);
            }
        });

        holder.ivPostImage.setVisibility(View.VISIBLE);
        if (URLUtil.isValidUrl(values.getImage()) ||
                Patterns.WEB_URL.matcher(values.getImage() != null ? values.getImage() : "").matches()) {
            try {
                picasso.load(values.getImage())
                        .fit().centerCrop().into(holder.ivPostImage, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError() {
                        holder.ivPostImage.setVisibility(View.GONE);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                holder.ivPostImage.setVisibility(View.GONE);
            }
        } else {
            holder.ivPostImage.setVisibility(View.GONE);
        }


        holder.tvPostTime.setText(DateFormatter.getTimeAgo(values.getUpdatedAt()));
        holder.tvPostTitle.setText(values.getTitle());

        if ("coupon".equals(values.getPostType())) {
            //TODO: Change this to visible when format is fixed on the server
            holder.tvPostEndsIn.setVisibility(View.GONE);
            holder.tvPostEndsIn.setText(values.getEndsIn());
        } else {
            holder.tvPostEndsIn.setVisibility(View.GONE);
        }

        holder.tvShortDescription.setText(values.getShortDescription());

        holder.singleItem.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (source.equals(BrandActivity.class.getSimpleName())) {
                    ((BrandActivity) activity).openDetailsActivity(values);
                } else {
                    ((TabsFragment) fragment).openDetailsActivity(values);
                }
            }
        });

        holder.singleItem.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder()
                        .setToolbarColor(ContextCompat.getColor(mContext, R.color.colorPrimary))
                        .setCloseButtonIcon(BitmapFactory.decodeResource(mContext.getResources(),
                                R.drawable.ic_arrow_back_white_24dp))
                        .build();
                CustomTabActivityHelper.openCustomTab(
                        (Activity) mContext, customTabsIntent, Uri.parse(values.getUrl()), new WebviewFallback());

                Answers.getInstance().logContentView(new ContentViewEvent()
                        .putContentName("WebViewActivity")
                        .putContentId(values.getId())
                        .putContentType(values.getPostType())
                        .putCustomAttribute("BrandId", values.getBrandId())
                        .putCustomAttribute("BrandId", values.getBrand().getName()));

                return true;
            }
        });

        final String finalBrandId = values.getBrand().getId();
        holder.followBrand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (source.equals(BrandActivity.class.getSimpleName())) {
                    ((BrandActivity) activity).followBrand(finalBrandId, holder.followBrand);
                } else {
                    ((TabsFragment) fragment).followBrand(finalBrandId, holder.followBrand);
                }
            }
        });

        holder.tvBrandName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("BrandClick", "name");
                if (!source.equals(BrandActivity.class.getSimpleName())) {
                    Log.i("BrandClick", "name");
                    Intent intent = new Intent(fragment.getActivity(), BrandActivity.class);
                    intent.putExtra(Constants.INTENT_BRAND_ID, finalBrandId);
                    intent.putExtra(Constants.INTENT_BRAND_NAME, values.getBrand().getName());
                    fragment.getActivity().startActivity(intent);
                }
            }
        });

        holder.brandPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("BrandClick", "pic");
                if (!source.equals(BrandActivity.class.getSimpleName())) {
                    Log.i("BrandClick", "pic");
                    Intent intent = new Intent(fragment.getActivity(), BrandActivity.class);
                    intent.putExtra(Constants.INTENT_BRAND_ID, finalBrandId);
                    intent.putExtra(Constants.INTENT_BRAND_NAME, values.getBrand().getName());
                    fragment.getActivity().startActivity(intent);
                }
            }
        });

        holder.tvPostTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("BrandClick", "time");
                if (!source.equals(BrandActivity.class.getSimpleName())) {
                    Log.i("BrandClick", "time");
                    Intent intent = new Intent(fragment.getActivity(), BrandActivity.class);
                    intent.putExtra(Constants.INTENT_BRAND_ID, finalBrandId);
                    intent.putExtra(Constants.INTENT_BRAND_NAME, values.getBrand().getName());
                    fragment.getActivity().startActivity(intent);
                }
            }
        });

        holder.tvBrandName.setText(values.getBrand().getName());

        int likesCount = Integer.parseInt(values.getLikesCount());
        holder.tvNumberOfLikes.setText(likesCount != 1 ?
                likesCount + " stars" : likesCount + " star");
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void addItems(List<Post> items) {

        int position = this.items.size();
        this.items.addAll(items);
        notifyItemRangeInserted(position, items.size());
    }

    public void clear() {
        this.items.clear();
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.iv_brand_pic)
        ImageView brandPic;

        @InjectView(R.id.tv_brand_name)
        TextView tvBrandName;

        @InjectView(R.id.tv_post_time)
        TextView tvPostTime;

        @InjectView(R.id.ib_follow)
        CustomImageButton followBrand;

        @InjectView(R.id.llSingleItem)
        LinearLayout singleItem;

        @InjectView(R.id.tv_short_description)
        TextView tvShortDescription;

        @InjectView(R.id.iv_post_image)
        ImageView ivPostImage;

        @InjectView(R.id.tv_post_title)
        TextView tvPostTitle;

        @InjectView(R.id.tv_post_price)
        TextView tvPostPrice;

        @InjectView(R.id.tv_post_ends_in)
        TextView tvPostEndsIn;

        @InjectView(R.id.tv_post_number_of_likes)
        TextView tvNumberOfLikes;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.inject(this, itemView);

            tvBrandName.setTypeface(typeface != null ? typeface : null);
            tvPostTime.setTypeface(typeface != null ? typeface : null);
            tvShortDescription.setTypeface(typeface != null ? typeface : null);
            tvPostTitle.setTypeface(typeface != null ? typeface : null);
            tvPostPrice.setTypeface(typeface != null ? typeface : null);
            tvPostEndsIn.setTypeface(typeface != null ? typeface : null);
            tvNumberOfLikes.setTypeface(typeface != null ? typeface : null);
        }
    }
}
