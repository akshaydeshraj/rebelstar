package in.adister.brandfeed.modules;

import dagger.Module;
import dagger.Provides;
import feed.data.rest.RestDataSource;
import in.adister.brandfeed.ui.login.LoginPresenter;
import in.adister.brandfeed.ui.login.LoginPresenterImpl;
import in.adister.brandfeed.ui.login.LoginView;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 08-Aug-15
 */

@Module
public class LoginModule {

    private LoginView view;

    public LoginModule(LoginView view) {
        this.view = view;
    }

    @Provides
    LoginView provideLoginView() {
        return view;
    }

    @Provides
    LoginPresenter provideLoginPresenter(LoginView loginView,
                                         RestDataSource restDataSource) {
        return new LoginPresenterImpl(view, restDataSource);
    }
}