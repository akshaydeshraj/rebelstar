package in.adister.brandfeed.modules;

import android.app.Application;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.util.Log;

import com.google.gson.Gson;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import feed.data.entities.BrandUser;
import feed.data.rest.RestDataSource;
import in.adister.brandfeed.BrandFeedApplication;
import in.adister.brandfeed.BuildConfig;
import in.adister.brandfeed.Constants;
import retrofit.Endpoint;
import retrofit.Endpoints;
import retrofit.RequestInterceptor;
import retrofit.client.Client;
import retrofit.client.OkClient;

/**
 * Created by zopper on 13/6/15.
 */

@Module
public class ApiModule {

    static final int DISK_CACHE_SIZE = 50 * 1024 * 1024; // 50 MB
    private static final String TAG = "ApiModule";
    public static long HTTP_TIMEOUT = 30 * 1000; // 30 seconds

    static OkHttpClient createOkHttpClient(Application app) {

        OkHttpClient client = new OkHttpClient();

        client.setConnectTimeout(HTTP_TIMEOUT, TimeUnit.MILLISECONDS);

        // Install an HTTP cache in the application cache directory.
        File cacheDir = new File(app.getCacheDir(), "http");
        Cache cache = new Cache(cacheDir, DISK_CACHE_SIZE);
        client.setCache(cache);

        return client;
    }

    @Provides
    public Endpoint provideEndpoint() {
        return Endpoints.newFixedEndpoint(BuildConfig.BASE_URL);
    }

    @Provides
    public OkHttpClient provideOkHttpClient(BrandFeedApplication application) {
        return createOkHttpClient(application);
    }

    @Provides
    public Client provideClient(OkHttpClient okHttpClient) {
        return new OkClient(okHttpClient);
    }

    @Provides
    public RestDataSource provideRestDataSource(Endpoint endpoint, Client client, final BrandFeedApplication app) {
        return new RestDataSource(endpoint, client, new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("device_id", Settings.Secure.getString(app.getContentResolver(), Settings.Secure.ANDROID_ID));
            }
        });
    }

    @Provides
    public Picasso providePicasso(BrandFeedApplication app, OkHttpClient client) {

        return new Picasso.Builder(app)
                .downloader(new OkHttpDownloader(client))
                .build();
    }

    @Provides
    public BrandUser provideBrandUser(SharedPreferences preferences) {

        Gson gson = new Gson();
        String json = preferences.getString(Constants.USER_BRAND, "");
        Log.e("BrandUser", json);
        BrandUser brandUser = gson.fromJson(json, BrandUser.class);

        if(brandUser == null){
            return new BrandUser(false);
        }else{
            return brandUser;
        }

    }
}
