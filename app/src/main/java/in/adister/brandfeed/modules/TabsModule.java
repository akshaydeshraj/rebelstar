package in.adister.brandfeed.modules;

import dagger.Module;
import dagger.Provides;
import feed.data.rest.RestDataSource;
import in.adister.brandfeed.ui.tabs.TabsPresenter;
import in.adister.brandfeed.ui.tabs.TabsPresenterImpl;
import in.adister.brandfeed.ui.tabs.TabsView;

/**
 * Created by zopper on 13/6/15.
 */

@Module
public class TabsModule {

    private TabsView tabsView;

    public TabsModule(TabsView tabsView){
        this.tabsView = tabsView;
    }

    @Provides TabsView provideTabsView(){
        return tabsView;
    }

    @Provides TabsPresenter provideTabsPresenter(TabsView tabsView, RestDataSource restDataSource){
        return new TabsPresenterImpl(tabsView, restDataSource);
    }

}
