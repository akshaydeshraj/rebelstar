package in.adister.brandfeed.modules;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import dagger.Module;
import dagger.Provides;
import in.adister.brandfeed.BrandFeedApplication;
import in.adister.brandfeed.CheckConnection;

/**
 * Created by zopper on 7/6/15.
 */

@Module
public class AppModule {

    private BrandFeedApplication application;

    SharedPreferences preferences;

    public AppModule(BrandFeedApplication application) {
        this.application = application;
    }

    @Provides
    public BrandFeedApplication provideApplication() {
        return application;
    }

    @Provides
    public SharedPreferences provideSharedPreferances() {
        preferences = PreferenceManager.getDefaultSharedPreferences(
                application.getApplicationContext());
        return preferences;
    }

    @Provides
    public SharedPreferences.Editor provideSharedPrefsEditor() {
        return preferences.edit();
    }

    @Provides
    public CheckConnection provideCheckConection(BrandFeedApplication application) {
        return new CheckConnection(application);
    }
}