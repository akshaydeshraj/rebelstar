package in.adister.brandfeed.modules;

import android.support.v7.app.AppCompatActivity;

import dagger.Module;
import dagger.Provides;
import in.adister.brandfeed.ui.main.MainView;

/**
 * Created by zopper on 7/6/15.
 */

@Module
public class MainModule {

    private MainView view;
    private AppCompatActivity activity;

    public MainModule(MainView view, AppCompatActivity activity){
        this.view = view;
        this.activity = activity;
    }

    @Provides public MainView provideView (){
        return view;
    }

}