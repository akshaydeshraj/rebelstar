package in.adister.brandfeed.utils.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 04-Nov-15
 */
public class FbUser implements Serializable{

    @SerializedName("id")
    private String id;

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("email")
    private String email;

    @SerializedName("name")
    private String name;

    // Getters and setters

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
