package com.adister.quickchat;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.adister.quickchat.core.ChatService;
import com.adister.quickchat.definitions.Consts;
import com.adister.quickchat.utils.DialogUtils;
import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBSession;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.QBSettings;
import com.quickblox.users.model.QBUser;

import java.util.List;


public class SignInActivity extends Activity implements View.OnClickListener {
    protected Context context;

    private EditText loginEditText;
    private EditText passwordEditText;
    protected ProgressDialog progressDialog;
    protected Button signInButton;

    @Override
    public void onCreate(Bundle savedInstanceBundle) {
        super.onCreate(savedInstanceBundle);
        setContentView(R.layout.activity_sign_in);
        context = this;
        progressDialog = DialogUtils.getProgressDialog(this);
        QBSettings.getInstance().fastConfigInit(Consts.APP_ID, Consts.AUTH_KEY, Consts.AUTH_SECRET);
        QBAuth.createSession(new QBEntityCallbackImpl<QBSession>() {
            @Override
            public void onSuccess(QBSession qbSession, Bundle bundle) {
                initUI();

            }

            @Override
            public void onError(List<String> errors) {
                // print errors that came from server
                DialogUtils.showLong(context, errors.get(0));
            }
        });

    }

    private void initUI() {
        loginEditText = (EditText) findViewById(R.id.login_edittext);
        passwordEditText = (EditText) findViewById(R.id.password_edittext);
        signInButton = (Button) findViewById(R.id.sign_in_button);
        if (signInButton != null) {
            signInButton.setOnClickListener(this);
        }

    }

    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.sign_in_button) {
            progressDialog.show();

            // Sign in application with user
            //
            final QBUser user = new QBUser();
            user.setLogin(loginEditText.getText().toString());
            user.setPassword(passwordEditText.getText().toString());

            ChatService.initIfNeed(this);

            ChatService.getInstance().login(user, new QBEntityCallbackImpl() {

                @Override
                public void onSuccess() {
                    // Go to Dialogs screen
                    //
                    Intent intent = new Intent(SignInActivity.this, DialogsActivityQuickblox.class);
                    startActivity(intent);
                    finish();
                }

                @Override
                public void onError(List errors) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(SignInActivity.this);
                    dialog.setMessage("chat login errors: " + errors).create().show();
                }
            });
        }
    }
}
