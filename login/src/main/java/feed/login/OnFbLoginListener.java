package feed.login;

import com.facebook.FacebookRequestError;

import org.json.JSONObject;

/**
 * @author akshay
 * @version 1.0.0
 * @since 3/7/15
 */
public interface OnFbLoginListener {

    void  onSuccess(JSONObject jsonObject);

    void onError(FacebookRequestError error);
}
