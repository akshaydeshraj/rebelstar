package feed.login.entities;

import com.google.android.gms.plus.model.people.Person;

import java.io.Serializable;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 29-Jul-15
 */
public class GoogleUser implements Serializable {

    private String id;

    private String firstName;

    private String lastName;

    private String email;
    private String name;
    private String locale;
    private String link;
    private String gender;
    private String imageUrl;

    public GoogleUser(Person person,String email) {

        setId(person.getId());
        setFirstName(person.getName().getGivenName());
        setLastName(person.getName().getFamilyName());
        setEmail(email);
        setName(person.getDisplayName());
        setLocale(person.getLanguage());
        setLink(person.getUrl());
        setGender(person.getGender());
        setImageUrl(person.getImage().getUrl());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(int gender) {
        switch (gender){
            case 0:
                this.gender = "male";
                this.gender = "female";
        }
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
