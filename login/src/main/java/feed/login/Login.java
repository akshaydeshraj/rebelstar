package feed.login;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import feed.login.entities.GoogleUser;

/**
 * @author akshay
 * @version 1.0.0
 * @since 3/7/15
 */
public class Login implements Application.ActivityLifecycleCallbacks{

    /* Request code used to invoke sign in user interactions. */
    public static final int GOOGLE_SIGN_IN = 0;
    private static final String TAG = "Login";
    /* Client used to interact with Google APIs. */
    private GoogleApiClient mGoogleApiClient;

    /* Is there a ConnectionResult resolution in progress? */
    private boolean mIsResolving = false;

    /* Should we automatically resolve ConnectionResults when possible? */
    private boolean mShouldResolve = false;

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

        if (activity.getLocalClassName().equalsIgnoreCase(
                "in.adister.brandfeed.ui.LoginActivity")){

            Log.v(TAG, "intializeClient");
            initializeGPlusApiClient(activity);
        }
    }

    @Override
    public void onActivityStarted(Activity activity) {

        if (activity.getLocalClassName().equalsIgnoreCase(
                "in.adister.brandfeed.ui.launcher.LoginActivity")){
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onActivityResumed(Activity activity) {

        if (activity.getLocalClassName().equalsIgnoreCase(
                "in.adister.brandfeed.ui.launcher.LoginActivity")){
        }
    }

    @Override
    public void onActivityPaused(Activity activity) {

        if (activity.getLocalClassName().equalsIgnoreCase(
                "in.adister.brandfeed.ui.launcher.LoginActivity")){
        }
    }

    @Override
    public void onActivityStopped(Activity activity) {

        if (activity.getLocalClassName().equalsIgnoreCase(
                "in.adister.brandfeed.ui.launcher.LoginActivity")){
        mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

        if (activity.getLocalClassName().equalsIgnoreCase(
                "in.adister.brandfeed.ui.launcher.LoginActivity")){
        }
    }

    @Override
    public void onActivityDestroyed(Activity activity) {

        if (activity.getLocalClassName().equalsIgnoreCase(
                "in.adister.brandfeed.ui.launcher.LoginActivity")){
        }
    }

    private void initializeGPlusApiClient(Activity activity){
        // Build GoogleApiClient with access to basic profile
        mGoogleApiClient = new GoogleApiClient.Builder(activity)
                .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks)activity)
                .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener)activity)
                .addApi(Plus.API)
                .addScope(new Scope(Scopes.PROFILE))
                .addScope(new Scope(Scopes.PLUS_LOGIN))
                .addScope(new Scope(Scopes.PLUS_ME))
                .addScope(new Scope(Scopes.EMAIL))
                .build();
    }

    public void onGplusSignInClicked(Activity activity){
        // User clicked the sign-in button, so begin the sign-in process and automatically
        // attempt to resolve any errors that occur.
        mShouldResolve = true;
        initializeGPlusApiClient(activity);
        mGoogleApiClient.connect();
    }

    public void onGplusActivityResult(int resultCode){
        // If the error resolution was not successful we should not resolve further.
        if (resultCode != Activity.RESULT_OK) {
            mShouldResolve = false;
        }

        mIsResolving = false;
        mGoogleApiClient.connect();
    }

    public GoogleUser onGplusConnected(Bundle bundle) {
        // onConnected indicates that an account was selected on the device, that the selected
        // account has granted any requested permissions to our app and that we were able to
        // establish a service connection to Google Play services.
        Log.d(TAG, "onConnected:" + bundle);
        mShouldResolve = false;

        if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null){
            Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
            String email = Plus.AccountApi.getAccountName(mGoogleApiClient);

            GoogleUser googleUser = new GoogleUser(currentPerson, email);

            return googleUser;
        }else {
            Log.v("GPLUS-PROFILE", "NO PERSON OBJECT");
            return null;
        }
    }

    public void onGPlusConnectionFailed(ConnectionResult connectionResult, Activity activity){


    }

    public void signOutOfGPlus(){
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
        }
    }

}