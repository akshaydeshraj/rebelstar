package feed.data.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 07-Nov-15
 */
public class StoriesFilterModel implements Cloneable {

    private static StoriesFilterModel ourInstance = new StoriesFilterModel();

    private List<String> categories = new ArrayList<>();

    private List<String> categoryPositions = new ArrayList<>();

    private StoriesFilterModel() {
        reset();
    }

    public static StoriesFilterModel getInstance() {
        return ourInstance;
    }

    public void reset(){
        categories = new ArrayList<>();
        categories.clear();
        categoryPositions = new ArrayList<>();
        categoryPositions.clear();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public boolean isEqualTo(StoriesFilterModel filterModel){

        return
                areListsEqual(this.categories, filterModel.categories) &&
                        areListsEqual(this.categoryPositions, filterModel.categoryPositions);
    }

    public boolean areListsEqual(List<String> list1, List<String> list2) {
        List<String> list = new ArrayList<>();

        if(list1.size() != list2.size()){
            return false;
        }

        for (String t : list1) {
            if(list2.contains(t)) {
                list.add(t);
            }
        }
        return list.size()==list1.size();
    }
    public void addElement(String categoryPosition){

        String categoryId = null;

        switch (categoryPosition){
            case "0":
                categoryId = String.valueOf(8);  //Brands
                break;
            case "1":
                categoryId = String.valueOf(9);  //Fun
                break;
            case "2":
                categoryId = String.valueOf(10);  //News
                break;
            case "3":
                categoryId = String.valueOf(11);  //Productivity
                break;
            case "4":
                categoryId = String.valueOf(12);  //Women
                break;
        }

        categories.add(categoryId);
        categoryPositions.add(categoryPosition);
    }

    public List<String> getCategories() {
        return categories;
    }

    public List<String> getCategoryPositions() {
        return categoryPositions;
    }
}
