package feed.data.entities;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 08-Aug-15
 */
public class BrandUser implements Serializable, Cloneable {

    private String id = "";

    @SerializedName("first_name")
    private String firstName = "";

    @SerializedName("last_name")
    private String lastName = "";

    private String email = "";

    private String password = "";

    private String phone = "";

    @SerializedName("is_active")
    private String isActive = "";

    @SerializedName("created_at")
    private String createdAt = "";

    @SerializedName("updated_at")
    private String updatedAt = "";

    @SerializedName("is_mobile_verified")
    private String isMobileVerified = "";

    private List<FollowedBrand> brands = new ArrayList<>();

    //TODO: Include this in the api
    private String imageUrl = "";
    private boolean loggedIn = false;

    public BrandUser(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public String getName() {
        return firstName + " " + lastName;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public boolean isEqualTo(BrandUser brandUser) {
        return
                this.id.equals(brandUser.id) &&
                        areListsEqual(this.brands, brandUser.brands);
    }

    public boolean areListsEqual(List<FollowedBrand> list1, List<FollowedBrand> list2) {

        if (list1.size() != list2.size()) {
            System.out.println("Size Not Equal");
            return false;
        }

        for (FollowedBrand t : list1) {
            if (!list2.contains(t)) {
                System.out.println(t.getBrandId() + " not found");
                return false;
            }
        }
        return true;
    }

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getPhone() {
        return phone;
    }

    public String getIsActive() {
        return isActive;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getIsMobileVerified() {
        return isMobileVerified;
    }

    public List<FollowedBrand> getBrands() {
        return brands;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public void addBrand(String brandId) {
        if (!this.brands.contains(new FollowedBrand(brandId))) {
            this.brands.add(new FollowedBrand(brandId));
        }
        showFollowedBrands();
    }

    public void removeBrand(String brandId) {

        for (int i = 0; i < brands.size(); i++) {
            if (brandId.equals(brands.get(i).getBrandId())) {
                brands.remove(i);
            }
        }
        showFollowedBrands();
    }

    public void showFollowedBrands() {

        for (FollowedBrand brand : brands) {
            Log.i("BrandUser", brand.getBrandId());
        }
    }

    public boolean contains(String brandId) {

        Log.i("BrandUser", brandId.trim());

        for (int i = 0; i < brands.size(); i++) {
            if (brandId.trim().equals(brands.get(i).getBrandId().trim())) {
                Log.i("BrandUserLoop", brands.get(i).getBrandId().trim());
                return true;
            }
        }

        return false;
    }

    public class FollowedBrand {

        @SerializedName("brand_id")
        private String brandId = "";

        public FollowedBrand(String brandId) {
            this.brandId = brandId;
        }

        public String getBrandId() {
            return brandId;
        }
    }
}