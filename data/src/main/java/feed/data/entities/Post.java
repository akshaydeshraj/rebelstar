package feed.data.entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 08-Aug-15
 */
public class Post implements Serializable, Comparable<Post> {

    private String title;
    private String id;

    @SerializedName("short_title")
    private String shortTitle;

    private String content;

    @SerializedName("link")
    private String url;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("start_date")
    private String startDate;

    @SerializedName("end_date")
    private String endDate;

    @SerializedName("brand_id")
    private String brandId;

    private String gender;

    @SerializedName("is_enabled")
    private String isEnabled;

    private String tags;

    private String image;

    private String mrp;

    @SerializedName("mrp_type")
    private String mrpType;

    @SerializedName("short_description")
    private String shortDescription;

    @SerializedName("ends_in")
    private String endsIn;

    @SerializedName("post_type")
    private String postType;

    private List<LikeObject> likes;

    private String likesCount;

    private boolean userLikesThisPost;

    @SerializedName("coupon_code")
    private String couponCode;

    private List<Brand> brand = new ArrayList<>();

    public class LikeObject implements Serializable {

        private String id;

        @SerializedName("user_id")
        private String userId;

        @SerializedName("coupon_id")
        private String couponId;

        @SerializedName("is_active")
        private String isActive;

        public LikeObject(String userId) {
            this.userId = userId;
        }
    }

    public class Brand implements Serializable {

        private String id;

        private String name;

        private String website;

        private String logo;

        @SerializedName("category_id")
        private String categoryId;

        private String description;

        private String tags;

        @SerializedName("is_enabled")
        private String isEnabled;

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getWebsite() {
            return website;
        }

        public String getLogo() {
            return logo;
        }

        public String getCategoryId() {
            return categoryId;
        }

        public String getDescription() {
            return description;
        }

        public String getTags() {
            return tags;
        }

        public String getIsEnabled() {
            return isEnabled;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public void setCategoryId(String categoryId) {
            this.categoryId = categoryId;
        }
    }

    public static Date formatTimestamp(String dateText) {
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
        Date date = null;
        try {
            date = originalFormat.parse(dateText);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }

    public String getShortTitle() {
        return shortTitle;
    }

    public String getContent() {
        return content;
    }

    public String getUrl() {
        return url;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getBrandId() {
        return brandId;
    }

    public String getGender() {
        return gender;
    }

    public String getIsEnabled() {
        return isEnabled;
    }

    public String getTags() {
        return tags;
    }

    public String getImage() {
        return image;
    }

    public String getMrp() {
        return mrp;
    }

    public String getMrpType() {
        return mrpType;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public String getEndsIn() {
        return endsIn;
    }

    public String getPostType() {
        return postType;
    }

    public List<LikeObject> getLikes() {
        return likes;
    }

    public String getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(String likesCount) {
        this.likesCount = likesCount;
    }

    public boolean isUserLikesThisPost() {
        return userLikesThisPost;
    }

    public void setUserLikesThisPost(boolean userLikesThisPost) {
        this.userLikesThisPost = userLikesThisPost;
    }

    public String getCouponCode() {
        return couponCode;
    }

    @Override
    public int compareTo(Post o) {
        return formatTimestamp(getCreatedAt()).compareTo(formatTimestamp(o.getCreatedAt()));
    }

    public void addLike(String userId) {
        likes.add(new LikeObject(userId));
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Brand getBrand() {
        return brand.get(0);
    }

    public void setBrand(String id, String image, String categoryId, String name) {

        Brand singleItem =  new Brand();
        singleItem.setId(id);
        singleItem.setLogo(image);
        singleItem.setCategoryId(categoryId);
        singleItem.setName(name);

        brand.add(singleItem);

    }
}
