package feed.data.entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 08-Aug-15
 */
public class Brand implements Serializable {

    private String id;

    private String name;

    @SerializedName("short_name")
    private String shortName;

    private String website;

    private String logo;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("is_enabled")
    private String isEnabled;

    @SerializedName("category_id")
    private String categoryId;

    private String description;

    private String tags;

    private String quickblox_id;

    public Brand(String id, String name, String logo, String categoryId) {
        this.id = id;
        this.name = name;
        this.logo = logo;
        this.categoryId = categoryId;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getShortName() {
        return shortName;
    }

    public String getWebsite() {
        return website;
    }

    public String getLogo() {
        return logo;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getIsEnabled() {
        return isEnabled;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public String getDescription() {
        return description;
    }

    public String getTags() {
        return tags;
    }

    public String getQuickbloxId() {
        return quickblox_id;
    }
}
