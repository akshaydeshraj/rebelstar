package feed.data.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 24-Aug-15
 */
public class FilterModel implements Cloneable{

    private static FilterModel ourInstance = new FilterModel();

    private List<String> categories = new ArrayList<>();

    private List<String> categoryPositions = new ArrayList<>();

    private FilterModel() {
        reset();
    }

    public static FilterModel getInstance() {
        return ourInstance;
    }

    public void reset(){
        categories = new ArrayList<>();
        categories.clear();
        categoryPositions = new ArrayList<>();
        categoryPositions.clear();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public boolean isEqualTo(FilterModel filterModel){

        return
                areListsEqual(this.categories, filterModel.categories) &&
                areListsEqual(this.categoryPositions, filterModel.categoryPositions);
    }

    public boolean areListsEqual(List<String> list1, List<String> list2) {
        List<String> list = new ArrayList<>();

        if(list1.size() != list2.size()){
            return false;
        }

        for (String t : list1) {
            if(list2.contains(t)) {
                list.add(t);
            }
        }
        return list.size()==list1.size();
    }
    public void addElement(String categoryPosition){

        String categoryId = null;

        switch (categoryPosition){
            case "0":
                categoryId = String.valueOf(1);  //Fashion and Beauty
                break;
            case "1":
                categoryId = String.valueOf(5);  //Electronics
                break;
            case "2":
                categoryId = String.valueOf(2);  //Rechaarge
                break;
            case "3":
                categoryId = String.valueOf(3);  //Food and Grocery
                break;
            case "4":
                categoryId = String.valueOf(6);  //Travel
                break;
            case "5":
                categoryId = String.valueOf(4);  //Home Decor
                break;
            case "6":
                categoryId = String.valueOf(7);  //Others
                break;
        }

        categories.add(categoryId);
        categoryPositions.add(categoryPosition);
    }

    public List<String> getCategories() {
        return categories;
    }

    public List<String> getCategoryPositions() {
        return categoryPositions;
    }
}