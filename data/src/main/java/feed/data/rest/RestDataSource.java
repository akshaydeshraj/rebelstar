package feed.data.rest;

import com.google.gson.GsonBuilder;

import java.util.Map;

import feed.data.OnRequestFinishedListener;
import retrofit.Callback;
import retrofit.Endpoint;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Client;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;
import rx.Observable;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 12-Jul-15
 */
public class RestDataSource {

    private BrandService brandService;
    private OnRequestFinishedListener listener;
    Callback retrofitCallback = new Callback() {
        @Override
        public void success(Object o, Response response) {

            listener.onSuccess(o, response);
        }

        @Override
        public void failure(RetrofitError error) {

            listener.onError(error);
        }
    };

    public RestDataSource(Endpoint endpoint, Client client, RequestInterceptor requestInterceptor) {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(endpoint)
                .setClient(client)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setConverter(new GsonConverter(new GsonBuilder().create()))
                .setRequestInterceptor(requestInterceptor)
                .build();

        brandService = restAdapter.create(BrandService.class);
    }

    public void setOnRequestFinishedListener(OnRequestFinishedListener listener) {
        this.listener = listener;
    }

    public void fetchData(String param, Map<String, String> queryMap) {

        brandService.getItems(param, queryMap, retrofitCallback);
    }

    public void addUser(String email, Map<String, String> queryMap) {

        brandService.addUser(email, queryMap, retrofitCallback);
    }

    public Observable<Response> followBrand(String userId, String brandId, boolean hasFollowed) {

        return brandService.followBrand(userId, brandId, hasFollowed ? String.valueOf(1) : String.valueOf(0));
    }

    public void likePost(String userId, String postId, String postType, boolean hasLiked) {

        brandService.likePost(userId, postId, postType, hasLiked ? String.valueOf(1) : String.valueOf(0)
                , retrofitCallback);
    }

    public void getBrandList() {
        brandService.getBrandList(retrofitCallback);
    }

    public void getPostsByBrand(Map<String, String> queryMap){
        brandService.getPostsByBrand(queryMap, retrofitCallback);
    }
}