package feed.data.rest;

import java.util.List;
import java.util.Map;

import feed.data.entities.Brand;
import feed.data.entities.BrandUser;
import feed.data.entities.Post;
import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.http.QueryMap;
import rx.Observable;

/**
 * @author Akshay
 * @version 1.0.0
 * @since 12-Jul-15
 */
public interface BrandService {

    @GET("/{param}/getAll?feeds=true")
    void getItems(
            @Path("param") String param,
            @QueryMap(encodeValues = false) Map<String, String> options,
            Callback<List<Post>> callback
    );

    @FormUrlEncoded
    @POST("/app/add/user")
    void addUser(
            @Field("email") String email,
            @FieldMap Map<String, String> params,
            Callback<BrandUser> callback
    );

    @GET("/app/{userId}/likes/{brandId}/brand")
    Observable<Response> followBrand(
            @Path("userId") String userId,
            @Path("brandId") String brandId,
            @Query("is_active") String hasFollowed
    );

    @GET("/app/{userId}/likes/{postId}/post")
    void likePost(
            @Path("userId") String userId,
            @Path("postId") String postId,
            @Query("post_type") String postType,
            @Query("is_active") String hasLiked,
            Callback<Response> callback
    );

    @GET("/brand/getAll")
    void getBrandList(Callback<List<Brand>> callback);

    @GET("/posts/getAll")
    void getPostsByBrand(
            @QueryMap Map<String, String> options,
            Callback<List<Post>> callback
    );
}
