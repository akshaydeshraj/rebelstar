package feed.data;


import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @author akshay
 * @version 1.0.0
 * @since 12/7/15
 */
public interface OnRequestFinishedListener {

    void onSuccess(Object response, Response retrofitResponse);

    void onError(RetrofitError error);
}
